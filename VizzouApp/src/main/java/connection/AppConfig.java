package connection;

/**
 * Created by ceino on 6/8/15.
 */
public class AppConfig {
    public static final String MYPREFERENCES = "MyPrefs" ;
    public static final String EMAIL = "emailkey";
    public static final String NAME = "nameKey";
    public static final String ID = "idKey";
}
