package data;

import java.io.Serializable;

/**
 * Created by ceino on 5/10/15.
 */
public class episode_videos implements Serializable {

    private String videoname;

    private String created_by;

    private String modified_on;

    private String keywords;

    private String created_on;

    private String tag;

    private String totalviewcount;

    private String userid;

    private String lastname;

    private String releasedate;

    private String firstname;

    private String imagename;

    private String episodeid;

    private String modified_by;

    private String episodename;

    private String videorefname;

    private String isTVicon;

    private String friendid;

    private boolean isWatched;

    private String feid;

    private String type;

    private String referredid;

    public String getReferredid() {
        return referredid;
    }

    public void setReferredid(String referredid) {
        this.referredid = referredid;
    }

    public String getFeid() {
        return feid;
    }

    public void setFeid(String feid) {
        this.feid = feid;
    }


    public boolean isWatched() {
        return isWatched;
    }

    public void setWatched(boolean watched) {
        isWatched = watched;
    }

    public String getisTVicon ()
    {
        return isTVicon;
    }

    public void setisTVicon (String isTVicon)
    {
        this.isTVicon = isTVicon;
    }


    public String getVideoname ()
    {
        return videoname;
    }

    public void setVideoname (String videoname)
    {
        this.videoname = videoname;
    }

    public String getCreated_by ()
    {
        return created_by;
    }

    public void setCreated_by (String created_by)
    {
        this.created_by = created_by;
    }

    public String getModified_on ()
    {
        return modified_on;
    }

    public void setModified_on (String modified_on)
    {
        this.modified_on = modified_on;
    }

    public String getKeywords ()
    {
        return keywords;
    }

    public void setKeywords (String keywords)
    {
        this.keywords = keywords;
    }

    public String getCreated_on ()
    {
        return created_on;
    }

    public void setCreated_on (String created_on)
    {
        this.created_on = created_on;
    }

    public String getTag ()
    {
        return tag;
    }

    public void setTag (String tag)
    {
        this.tag = tag;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getTotalviewcount ()
    {
        return totalviewcount;
    }

    public void setTotalviewcount (String totalviewcount)
    {
        this.totalviewcount = totalviewcount;
    }

    public String getFriendid ()
    {
        return friendid;
    }

    public void setFriendid (String friendid)
    {
        this.friendid = friendid;
    }

    public String getUserid ()
    {
        return userid;
    }

    public void setUserid (String userid)
    {
        this.userid = userid;
    }

    public String getLastname ()
    {
        return lastname;
    }

    public void setLastname (String lastname)
    {
        this.lastname = lastname;
    }

    public String getReleasedate ()
    {
        return releasedate;
    }

    public void setReleasedate (String releasedate)
    {
        this.releasedate = releasedate;
    }

    public String getFirstname ()
    {
        return firstname;
    }

    public void setFirstname (String firstname)
    {
        this.firstname = firstname;
    }

    public String getImagename ()
    {
        return imagename;
    }

    public void setImagename (String imagename)
    {
        this.imagename = imagename;
    }

    public String getEpisodeid ()
    {
        return episodeid;
    }

    public void setEpisodeid (String episodeid)
    {
        this.episodeid = episodeid;
    }

    public String getModified_by ()
    {
        return modified_by;
    }

    public void setModified_by (String modified_by)
    {
        this.modified_by = modified_by;
    }

    public String getEpisodename ()
    {
        return episodename;
    }

    public void setEpisodename (String episodename)
    {
        this.episodename = episodename;
    }

    public String getVideorefname ()
    {
        return videorefname;
    }

    public void setVideorefname (String videorefname)
    {
        this.videorefname = videorefname;
    }
}
