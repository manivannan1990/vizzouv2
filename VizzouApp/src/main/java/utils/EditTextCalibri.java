package utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by ceino on 8/12/15.
 */
public class EditTextCalibri extends EditText {

    public EditTextCalibri(Context context) {
        super(context);
        init(context);
    }

    public EditTextCalibri(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public EditTextCalibri(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }
    private void init(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "fonts/Calibri.ttf");
        setTypeface(tf);
    }
}
