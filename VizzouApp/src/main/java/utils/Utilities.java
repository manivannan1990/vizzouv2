package utils;

import android.content.Context;
import android.util.TypedValue;

import java.net.URI;
import java.util.Random;

public class Utilities {
	public static int generateRandomNumber()
	{
		int min = 20;
		int max = 99999;

		Random r = new Random();
		int i1 = r.nextInt(max - min + 1) + min;
		return i1 ;
	}
	
	
	public static class Constants{

		
		public static class LogConstants
		{
			public static final String APP_FLOW="App_Flow";
			public static final String API = "API";
			
		}
		
		
		
		public  static class PreferenceKeys
		{
			public static final String IS_ON_BOARD_DONE="isonboarddone";
			public static final String VIZZOU_ACCESS_USERID="vizzouAccessUserId";
			public static final String VIZZOU_ACCESS_EMAIL="vizzouAccessEmail";
			public static final String VIZZOU_ACCESS_UNAME="vizzouAccessUname";
			public static final String VIZZOU_ACCESS_PWD="vizzouAccessPwd";
			public static final String VIZZOU_ACCESS_IMAGE="vizzouAccessimage";
			public static final String VIZZOU_ACCESS_FNAME="vizzouAccessfname";
			public static final String VIZZOU_ACCESS_LNAME="vizzouAccesslname";
			public static final String VIZZOU_ACCESS_STREET="vizzouAccessstreet";
			public static final String VIZZOU_ACCESS_DESC="vizzouAccessdesc";
			public static final String VIZZOU_ACCESS_PHONE="vizzouAccessphone";
		}

		public  static class URLChecker
		{
			public static boolean isURI( String str ) {

			    if ( str.indexOf( ':' ) == -1 )       return false;
			    str = str.toLowerCase().trim();

			    if ( !str.startsWith( "http://" )
			      && !str.startsWith( "https://" )
			      && !str.startsWith( "file://" )
			      && !str.startsWith( "ftp://" )
			      && !str.startsWith( "mailto:" )
			      && !str.startsWith( "news:" )
			      && !str.startsWith( "urn:" ) )      return false;

			    try {

			        URI uri = new URI( str );
			        String proto = uri.getScheme();

			        if ( proto == null )              return false;

			        if ( proto.equals( "http" ) || proto.equals( "https" ) || proto.equals( "file" ) || proto.equals( "ftp" ) ) {

			            if ( uri.getHost() == null )  return false;

			            String path = uri.getPath();
			            if ( path != null ) {

			                int len = path.length();
			                for ( int i=0; i<len; i++ ) {

			                    if ( "?<>:*|\"".indexOf( path.charAt( i ) ) > -1 )
			                        return false;
			                }
			            }
			        }

			        return true;
			    } catch ( Exception ex ) {

			        return false;
			    }
			}
			
		}
	}


	public static int convertToDp(int i,Context context) {
		
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, context.getResources().getDisplayMetrics());
	}

}
