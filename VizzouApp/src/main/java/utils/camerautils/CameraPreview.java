package utils.camerautils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.util.Log;
import android.util.TypedValue;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.LinearLayout;

import java.io.IOException;
import java.util.List;


/**
 *
 * Vista personalizada para la previsualización del video
 *
 * @author anthorlop
 *
 */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

	private static final String TAG = "CameraPreview";

	private SurfaceHolder mHolder;
	private Camera mCamera;

	private Context mContext;

	int actionBarHeight;
	private String flashMode =  Camera.Parameters.FLASH_MODE_OFF;

	public CameraPreview(Context context, Camera camera) {
		super(context);
		mCamera = camera;

		mContext = context;
		mHolder = getHolder();
		mHolder.addCallback(this);
		// deprecated setting, but required on Android versions prior to 3.0
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	public void surfaceCreated(SurfaceHolder holder) {
		try {
			// create the surface and start camera preview
			if (mCamera != null) {
				mCamera.setPreviewDisplay(holder);
				optimizeCameraDimens(mCamera);
				mCamera.startPreview();
			}
		} catch (IOException e) {
			Log.d(VIEW_LOG_TAG, "Error setting camera preview: " + e.getMessage());
		}
	}

	public void refreshCamera(Camera camera) {
		if (mHolder.getSurface() == null) {
			// preview surface does not exist
			return;
		}
		// stop preview before making changes
		try {
			mCamera.stopPreview();
		} catch (Exception e) {
			// ignore: tried to stop a non-existent preview
		}
		// set preview size and make any resize, rotate or
		// reformatting changes here
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
			if (camera != null) {
				camera.setDisplayOrientation(90);
			}
		}

		// start preview with new settings
		setCamera(camera);
		try {
			mCamera.setPreviewDisplay(mHolder);

			optimizeCameraDimens(mCamera);

			mCamera.startPreview();
		} catch (Exception e) {
			Log.d(VIEW_LOG_TAG, "Error starting camera preview: " + e.getMessage());
		}
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		// If your preview can change or rotate, take care of those events here.
		// Make sure to stop the preview before resizing or reformatting it.
		refreshCamera(mCamera);
	}

	public void setCamera(Camera camera) {
		//method to set a camera instance
		mCamera = camera;
//		setCameraDisplayOrientation(((Activity) mContext), 0, mCamera);

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		// mCamera.release();

	}

	private void optimizeCameraDimens(Camera mCamera) {
		final int width = Screens.getScreenWidth(mContext);
		final int height = Screens.getScreenHeight(mContext);

		Log.e("Screen", width + "/" + height);

		final List<Camera.Size> mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
		for(Camera.Size str: mSupportedPreviewSizes)
			Log.e(TAG, str.width + "/" + str.height);

		Camera.Size mPreviewSize;

		if (mSupportedPreviewSizes != null) {
			mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height+getStatusBarHeight());
			Log.e("Screen", " actionBarHeight:" + getStatusBarHeight());
			float ratio;
			if(mPreviewSize.height >= mPreviewSize.width)
				ratio = (float) mPreviewSize.height / (float) mPreviewSize.width;
			else
				ratio = (float) mPreviewSize.width / (float) mPreviewSize.height;

			// One of these methods should be used, second method squishes preview slightly
			setMeasuredDimension(width, (int) (width * ratio));

			Camera.Parameters parameters = mCamera.getParameters();
			parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
			Log.e("sizes", "width:" + width + " height:" + height + " mPreviewSizewidth:" + mPreviewSize.width + " mPreviewSizeheight:" + mPreviewSize.height);
			parameters.setFlashMode(flashMode);
			mCamera.setParameters(parameters);
		}
	}

	public int getStatusBarHeight() {
		int result = 0;
		int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = getResources().getDimensionPixelSize(resourceId);
		}
		return result;
	}

	private Camera.Size getOptimalSize(List<Camera.Size> sizes, int w, int h) {

		final double ASPECT_TOLERANCE = 0.2;
		w = 720;
		h = 1280;
		double targetRatio = (double) w / h;
		if (sizes == null)
			return null;
		Camera.Size optimalSize = null;
		double minDiff = Double.MAX_VALUE;
		int targetHeight = h;
		// Try to find an size match aspect ratio and size
		for (Camera.Size size : sizes)
		{
//          Log.d("CameraActivity", "Checking size " + size.width + "w " + size.height + "h");
			double ratio = (double) size.width / size.height;
			if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
				continue;
			if (Math.abs(size.height - targetHeight) < minDiff)
			{
				optimalSize = size;
				minDiff = Math.abs(size.height - targetHeight);
			}
		}
		// Cannot find the one match the aspect ratio, ignore the requirement

		if (optimalSize == null)
		{
			minDiff = Double.MAX_VALUE;
			for (Camera.Size size : sizes) {
				if (Math.abs(size.height - targetHeight) < minDiff)
				{
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}
		}

		/*SharedPreferences previewSizePref;
		if (cameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
			previewSizePref = getSharedPreferences("PREVIEW_PREF",MODE_PRIVATE);
		} else {
			previewSizePref = getSharedPreferences("FRONT_PREVIEW_PREF",MODE_PRIVATE);
		}

		SharedPreferences.Editor prefEditor = previewSizePref.edit();
		prefEditor.putInt("width", optimalSize.width);
		prefEditor.putInt("height", optimalSize.height);

		prefEditor.commit();*/

      Log.d("CameraActivity", "Using size: " + optimalSize.width + "w " + optimalSize.height + "h");
		return optimalSize;
	}

	/*@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

		final int width = Screens.getScreenWidth(mContext);
		final int height = Screens.getScreenHeight(mContext);

		final List<Camera.Size> mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
		for(Camera.Size str: mSupportedPreviewSizes)
			Log.e(TAG, str.width + "/" + str.height);

		Camera.Size mPreviewSize;

		if (mSupportedPreviewSizes != null) {
			mPreviewSize = getOptimalSize(mSupportedPreviewSizes, width, height);
			float ratio;
			if(mPreviewSize.height >= mPreviewSize.width)
				ratio = (float) mPreviewSize.height / (float) mPreviewSize.width;
			else
				ratio = (float) mPreviewSize.width / (float) mPreviewSize.height;

			// One of these methods should be used, second method squishes preview slightly
			setMeasuredDimension(width, (int) (width * ratio));

		}
	}
*/

	private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
		final double ASPECT_TOLERANCE = 0.1;
		double targetRatio = (double) h / w;

		if (w > h)
			targetRatio = (double) w / h;

		if (sizes == null)
			return null;

		Camera.Size optimalSize = null;
		double minDiff = Double.MAX_VALUE;

		int targetHeight = h;

		for (Camera.Size size : sizes) {

			double ratio = (double) size.width / size.height;
			if(size.height >= size.width)
				ratio = (float) size.height/size.width;

			if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
				continue;

			if (Math.abs(size.height - targetHeight) < minDiff) {
				optimalSize = size;
				minDiff = Math.abs(size.height - targetHeight);
			}
		}

		if (optimalSize == null) {
			minDiff = Double.MAX_VALUE;
			for (Camera.Size size : sizes) {
				if (Math.abs(size.height - targetHeight) < minDiff) {
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}
		}

		return optimalSize;
	}

	public static void setCameraDisplayOrientation(Activity activity, int cameraId, android.hardware.Camera camera) {
		android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
		android.hardware.Camera.getCameraInfo(cameraId, info);
		int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
		int degrees = 0;
		switch (rotation) {
			case Surface.ROTATION_0:
				degrees = 0;
				break;
			case Surface.ROTATION_90:
				degrees = 90;
				break;
			case Surface.ROTATION_180:
				degrees = 180;
				break;
			case Surface.ROTATION_270:
				degrees = 270;
				break;
		}

		int result;
		//int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		// do something for phones running an SDK before lollipop
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360; // compensate the mirror
		} else { // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}

		camera.setDisplayOrientation(result);
	}

	public void setFlashMode(String mode) {
		flashMode = mode;
	}
}