package utils.camerautils;

/**
 * Created by karthikn on 6/1/2016.
 */
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.dev.vizzou.R;
import com.dev.vizzou.activities.UploadActivity;
import com.dev.vizzou.common.AppConstants;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PhotoHandler implements Camera.PictureCallback {

    private final Context context;
    private final String userid;

    private final String TAG = "PhotoHandler";

    public PhotoHandler(Context context, String userid) {
        this.context = context;
        this.userid = userid;
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        new SaveImageTask(context).execute(data);
        camera.startPreview();
    }

    /**
     * Create a File for saving an image or video
     */
    private File getImageFile() {
        File picDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath() + "/"+context.getResources().getString(R.string.app_name));

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("CameraTry", "Fail creating directory");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String mediaFileName = mediaStorageDir.getAbsolutePath() +
                File.separator +
                "IMG_" + timeStamp + ".jpg";
        File mediaFile = new File(mediaFileName);

        return mediaFile;
    }

    private void refreshGallery(Uri fileUri) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(fileUri);
        context.sendBroadcast(mediaScanIntent);
    }

    private class SaveImageTask extends AsyncTask<byte[], Void, File> {

        private ProgressDialog pDialog;
        private Context contexto;

        public SaveImageTask(Context contexto) {
            this.contexto = contexto;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(contexto);
            pDialog.setMessage("Saving photo");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(true);
            pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    cancel(true);
                }
            });
            pDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(File pictureFile) {
            String message;

            pDialog.dismiss();
            refreshGallery(Uri.fromFile(pictureFile));
            if (pictureFile == null) {
                message = "Error creating media file, check storage permissions";
            } else {
                message = "Photo saved in: " + Uri.fromFile(pictureFile);
                Log.e("filePAth",Uri.fromFile(pictureFile).toString());
                Intent i = new Intent(context, UploadActivity.class);
                i.putExtra(AppConstants.intent_filePath, pictureFile.getAbsolutePath());
                i.putExtra(AppConstants.intent_previewPath, Uri.fromFile(pictureFile).toString());
                i.putExtra(AppConstants.user_id, userid);
                i.putExtra(AppConstants.is_Camera, true);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                ((Activity)context).startActivity(i);
            }
//            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected File doInBackground(byte[]... data) {
            File pictureFile = getImageFile();
            /*try {
                File f = getImageFile();
                ExifInterface exif = new ExifInterface(f.getPath());
                int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

                int angle = 0;

                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    angle = 90;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    angle = 180;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    angle = 270;
                }

                Matrix mat = new Matrix();
                mat.postRotate(angle);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;

                Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f),
                        null, options);
                bitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
                        bmp.getHeight(), mat, true);
                ByteArrayOutputStream outstudentstreamOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100,
                        outstudentstreamOutputStream);
                imageView.setImageBitmap(bitmap);

            } catch (IOException e) {
                Log.w("TAG", "-- Error in setting image");
            } catch (OutOfMemoryError oom) {
                Log.w("TAG", "-- OOM Error in setting image");
            }*/
            if (pictureFile == null) {
                Log.d(TAG, "Error creating media file, check storage permissions");
                return null;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data[0]);
                fos.close();
            } catch (FileNotFoundException e) {
                Log.d(TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d(TAG, "Error accessing file: " + e.getMessage());
            }
            return pictureFile;
        }
    }

    private Bitmap imageOreintationValidator(Bitmap bitmap, String path) {

        ExifInterface ei;
        try {
            ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotateImage(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotateImage(bitmap, 180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    bitmap = rotateImage(bitmap, 270);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    private Bitmap rotateImage(Bitmap source, float angle) {

        Bitmap bitmap = null;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        try {
            bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                    matrix, true);
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
        }
        return bitmap;
    }
}