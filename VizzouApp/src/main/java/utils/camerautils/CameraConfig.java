package utils.camerautils;


public class CameraConfig {

    public static int MAX_DURATION_RECORD = 60000;
    public static int MAX_FILE_SIZE_RECORD = 50000000;

}
