package utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by ceino on 8/12/15.
 */
public class TextViewOpenSansLight extends TextView {

    public TextViewOpenSansLight(Context context) {
        super(context);
        init(context);
    }

    public TextViewOpenSansLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TextViewOpenSansLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }
    private void init(Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "fonts/opensans_light.ttf");
        setTypeface(tf);
    }
}
