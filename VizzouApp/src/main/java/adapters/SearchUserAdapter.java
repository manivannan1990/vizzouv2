package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dev.vizzou.R;
import com.dev.vizzou.common.AppConstants;
import com.dev.vizzou.pojo.Friendsitem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import utils.Utils;


public class SearchUserAdapter extends RecyclerView.Adapter<SearchUserAdapter.ViewHolder> {

    List<Friendsitem> list = new ArrayList<>();
    Context context;

    public SearchUserAdapter(List<Friendsitem> list) {
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_user_item, parent, false);
        context = parent.getContext();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Friendsitem friend = list.get(position);
        holder.Friendsitem = getItem(position);
        holder.usrName.setText(friend.getFirstName()+" "+friend.getLastName());
        if(Utils.isValid(friend.getProfPicture())) {
            String temp = friend.getProfPicture();
            temp = temp.replaceAll(" ", "%20");
            Picasso.with(context).load(AppConstants.IMAGE_URL + temp).placeholder(R.drawable.placeholderuser)
                    .error(R.drawable.nointernet)
                    .fit()
                    .into(holder.usrImage);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public Friendsitem getItem(int i) {
        return list.get(i);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        utils.CircleImageView usrImage;
        TextView usrName;
        Friendsitem Friendsitem;

        public ViewHolder(View itemView) {
            super(itemView);
            usrImage = (utils.CircleImageView) itemView.findViewById(R.id.usrimage);
            usrName = (TextView) itemView.findViewById(R.id.usrname);
        }


    }
}