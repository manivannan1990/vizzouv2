package adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.dev.vizzou.R;
import com.dev.vizzou.activities.EditOpenEpisodeFriends;
import com.dev.vizzou.common.AppConstants;
import com.dev.vizzou.videostream.VideoPlayerActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import data.episode_videos;

public class RecyclerView_Adapter extends
        RecyclerView.Adapter<RecyclerView_Adapter.RecyclerViewHolder> {// Recyclerview will extend to
    // recyclerview adapter
    private ArrayList<episode_videos> arrayList;
    private Context context;
    String flag;
    String userid;

    public RecyclerView_Adapter(Context context, ArrayList<episode_videos> arrayList, String userid, String flag) {
        this.context = context;
        this.userid = userid;
        this.flag = flag;
        this.arrayList = arrayList;

    }
 
    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
 
    }
 
    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        final episode_videos model = arrayList.get(position);

        final RecyclerViewHolder mainHolder = (RecyclerViewHolder) holder;// holder

 
        // setting title
        mainHolder.title.setText(model.getEpisodename());
        mainHolder.username.setText(model.getVideoname());
        if(model.getisTVicon().equalsIgnoreCase("1")){
            mainHolder.tvicon.setVisibility(View.GONE);
        }


        Picasso.with(context).load(AppConstants.IMAGE_URL+model.getImagename())
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(mainHolder.imageview);

        //Seekbar
        mainHolder.timeline.setProgress(AppConstants.getPercentageLeft(model.getReleasedate()));


//        ViewTreeObserver viewTreeObserver = mainHolder.episode_item_view.getViewTreeObserver();
//        if (viewTreeObserver.isAlive()) {
//            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
//                @Override
//                public void onGlobalLayout() {
//                    mainHolder.episode_item_view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//
//                    System.out.println("height:"+mainHolder.episode_item_view.getWidth() + mainHolder.episode_item_view.getHeight() );
//                    int k = (int)((AppConstants.getPercentageLeft(model.getReleasedate())/100.0)*mainHolder.episode_item_view.getWidth());
//                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(k, 10);
//                    lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                    mainHolder.timeline.setLayoutParams(lp);
//                }
//            });
//        }



        mainHolder.episode_item_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(model.getisTVicon().equalsIgnoreCase("1")){
                    if(flag.equalsIgnoreCase(AppConstants.GETEPISODESFRIENDS)){

                    }
                    Intent intent = new Intent(context,EditOpenEpisodeFriends.class);
                    intent.putExtra(AppConstants.user_id,userid);
                    ((Activity)context).startActivity(intent);
                }else{
                    String url = AppConstants.VIDEO_URL+model.getVideoname();
                    Intent intent = new Intent(context,VideoPlayerActivity.class);
                    intent.putExtra("URL",url);
                    ((Activity)context).startActivity(intent);
                }

            }
        });


    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
 
        // This method will inflate the custom layout and return as viewholder
        LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());
 
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(
                R.layout.item_row, viewGroup, false);
        RecyclerViewHolder listHolder = new RecyclerViewHolder(mainGroup);
        return listHolder;
 
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // View holder for gridview recycler view as we used in listview
        public TextView title,username;
        public utils.ProportionalImageView imageview;
        public ImageView tvicon;
        public SeekBar timeline;
//        public View timeline;
        public LinearLayout episode_item_view;

        public RecyclerViewHolder(View view) {
            super(view);
            // Find all views ids

            this.title = (TextView) view
                    .findViewById(R.id.title);
            this.username = (TextView) view
                    .findViewById(R.id.username);
            this.imageview = (utils.ProportionalImageView) view
                    .findViewById(R.id.image);
            this.tvicon = (ImageView) view.findViewById(R.id.tvicon);
//            this.timeline = (View) view.findViewById(R.id.timeline);
            this.timeline = (SeekBar) view.findViewById(R.id.timeline);
            this.episode_item_view = (LinearLayout) view.findViewById(R.id.episode_item_view);
            view.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
//            Toast.makeText(v.getContext(), "clickingggg", Toast.LENGTH_SHORT).show();
        }
    }

}