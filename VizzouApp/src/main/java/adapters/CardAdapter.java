package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.dev.vizzou.R;
import com.dev.vizzou.common.AppConstants;
import com.dev.vizzou.pojo.Friendsitem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import utils.Utils;


public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {

    List<Friendsitem> list = new ArrayList<>();
    Context context;

    public CardAdapter(List<Friendsitem> list) {
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);
        context = parent.getContext();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final int pos = position;
        final Friendsitem friend = list.get(position);
        holder.Friendsitem = getItem(position);
        holder.usrName.setText(friend.getFirstName() + " " + friend.getLastName());
//        holder.usrImage.setImageResource(R.drawable.image2);
        if(Utils.isValid(friend.getProfPicture())) {
            final String temp = friend.getProfPicture().replaceAll(" ", "%20");
            Picasso.with(context).load(AppConstants.IMAGE_URL + temp).placeholder(R.drawable.placeholderuser)
                    .error(R.drawable.nointernet)
                    .fit()
                    .into(holder.usrImage);
        }
        holder.chkSelected.setChecked(list.get(position).isUsrSelected());

        holder.chkSelected.setTag(list.get(position));


        holder.chkSelected.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                Friendsitem contact = (Friendsitem) cb.getTag();

                contact.setUsrSelected(cb.isChecked());
                list.get(pos).setUsrSelected(cb.isChecked());
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public Friendsitem getItem(int i) {
        return list.get(i);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        utils.CircleImageView usrImage;
        CheckBox chkSelected;
        TextView usrName;
        Friendsitem Friendsitem;

        public ViewHolder(View itemView) {
            super(itemView);
            usrImage = (utils.CircleImageView) itemView.findViewById(R.id.usrimage);
            usrName = (TextView) itemView.findViewById(R.id.usrname);
            chkSelected = (CheckBox) itemView.findViewById(R.id.chkSelected);
        }
    }

    public List<Friendsitem> getFriendsList() {
        return list;
    }
}