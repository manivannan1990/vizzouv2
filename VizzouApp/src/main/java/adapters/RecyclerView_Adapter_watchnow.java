package adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dev.vizzou.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import data.DataModel;

public class RecyclerView_Adapter_watchnow extends
        RecyclerView.Adapter<RecyclerView_Adapter_watchnow.RecyclerViewHolder> {// Recyclerview will extend to
    // recyclerview adapter
    private ArrayList<DataModel> arrayList;
    private Context context;

    public RecyclerView_Adapter_watchnow(Context context,
                                         ArrayList<DataModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
 
    }
 
    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);
 
    }
 
    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        final DataModel model = arrayList.get(position);
 
        RecyclerViewHolder mainHolder = (RecyclerViewHolder) holder;// holder

        Bitmap image = BitmapFactory.decodeResource(context.getResources(),
                model.getImage());// This will convert drawbale image into
        // bitmap
 
        // setting title
        mainHolder.title.setText(model.getTitle());
        Picasso.with(context).load(model.getImage())
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(mainHolder.imageview);

//        mainHolder.imageview.setImageBitmap(image);



    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
 
        // This method will inflate the custom layout and return as viewholder
        LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());
 
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(
                R.layout.item_row_watchnow, viewGroup, false);
        RecyclerViewHolder listHolder = new RecyclerViewHolder(mainGroup);
        return listHolder;
 
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // View holder for gridview recycler view as we used in listview
        public TextView title,username;
        public utils.ProportionalImageView imageview;




        public RecyclerViewHolder(View view) {
            super(view);
            // Find all views ids

            this.title = (TextView) view
                    .findViewById(R.id.title);
            this.username = (TextView) view
                    .findViewById(R.id.username);
            this.imageview = (utils.ProportionalImageView) view
                    .findViewById(R.id.image);
            view.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
//            Toast.makeText(v.getContext(), "clickingggg_watnow", Toast.LENGTH_SHORT).show();
        }
    }
 
}