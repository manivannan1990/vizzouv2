package com.dev.vizzou.common;

import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by karthik on 25/9/15.
 */
public class AppConstants {
    public Context context;

    public static String BASE_URL = "http://52.32.183.109:8080/vizzou/api/v1.0/";
    public static String IMAGE_URL = "http://52.32.183.109:8080/images/";
    public static String VIDEO_URL = "http://52.32.183.109:8080/videos/";

    public static String ACTION_LOGIN = "user/userLogin";
    public static String ACTION_REGISTER = "user/userRegistration";
    public static String ACTION_CREATEEPISODE = "episode/createEpisode";
    public static String ACTION_CREATEFRIENDSEPISODE = "episode/createFriendsEpisode";
    public static String ACTION_GETALLUSER = "user/searchFriends";
    public static String ACTION_GETEPISODES = "episode/getEpisodes";
    public static String ACTION_GETEPISODESFRIENDS = "episode/getVideoSharedByFriends";
    public static String ACTION_GETEPISODESOPEN = "episode/getOpenEpisodes";
    public static String ACTION_WATCHNOW = "episode/getAllWatchListVideos";
    public static String ACTION_ADDWATCHNOW = "episode/addToWatchNowList";
    public static String ACTION_UPDATEWATCHNOW = "episode/updateWatchNowList";
    public static String ACTION_REMOVEWATCHNOW = "episode/removeInWatchNowList";
    public static String ACTION_SEARCHFRIENDS = "user/searchFriends";
    public static String ACTION_SEARCHEPISODES = "episode/searchOpenEpisodes";
    public static String ACTION_VIEWUSERPROFILE = "user/viewUserProfile";
    public static String ACTION_GETUSERPROFILE = "user/getUserProfile";
    public static String ACTION_UPDATEUSER = "user/updateUser";
    public static String ACTION_JOINOPENEPISODE = "episode/joinOpenEpisode";
    public static String ACTION_UNJOINOPENEPISODE = "episode/unJoinOpenEpisode";
    public static String ACTION_FOLLOWFRIEND = "user/followFriend";
    public static String ACTION_ACCEPTFRIEND = "user/accept";
    public static String ACTION_UNFLLOWFRIEND = "user/unFollowFriend";


    public static String UPLOAD = "UPLOAD";
    public static String UPLOADTEMP = "UPLOADTEMP";
    public static String UPLOADIMAGE = "UPLOADIMAGE";
    public static String intent_filePath = "FILE_URL_PATH";
    public static String intent_previewPath = "FILE_URL_PREVIEW_PATH";
    public static String user_id = "USERID";
    public static String is_Camera = "IS_CAMERA";
    public static String isLogged = "isLogged";
    public static String LOGIN = "LOGIN";
    public static String JOIN = "CALL_JOIN";
    public static String UNJOIN = "CALL_UNJOIN";
    public static String EPISODE_ID = "EPISODEID";
    public static String FRIEND_ID = "FRIENDID";
    public static String USR_ID = "USRID";
    public static String FE_ID = "FEID";
    public static String FID = "FID";
    public static String FLAG = "FLAG";
    public static String TYPE = "TYPE";
    public static String REFERRED_ID = "REFERREDID";
    public static String REGISTER = "REGISTER";
    public static String VIEW_USER_PROFILE = "VIEW_USER_PROFILE";
    public static String UPDATE_USER = "UPDATE_USER";
    public static String GETEPISODES = "PRIVATE";
    public static String GETEPISODESOPEN = "MyEpisodeOpen";
    public static String GETWATCHNOW = "WatchNow";
    public static String REMOVEWATCHNOW = "RemoveWatchNow";
    public static String GETEPISODESFRIENDS = "MyEpisodeFriends";
    public static String FOLLOWFRIEND = "followfriend";
    public static String ACCEPTFRIEND = "acceptfriend";
    public static String UNFOLLOWFRIEND = "unfollowfriend";
    public static String FRIENDSLIST = "FRIENDSLIST";



    public static int getresid(Context context, String idStr, String defType) {
        int resId = context.getResources().getIdentifier(idStr, defType, context.getPackageName());
        return resId;
    }

    public static void setSnackbar(View view, String str) {
        Snackbar.make(view, str, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    public static void hideSoftKeyboard(Context context, View v) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static int getresId(Context context, String toStr, String idStr) {
        int resId = context.getResources().getIdentifier(idStr, toStr, context.getPackageName());
        return resId;
    }

    public static int getresIDStr(Context context, String idStr) {
        int resId = context.getResources().getIdentifier(idStr, "string", context.getPackageName());
        return resId;
    }

    public static String checkNullStr(String idStr) {
        String retStr = "";
        if (idStr.isEmpty() || idStr.equalsIgnoreCase(null) || idStr.equalsIgnoreCase("null"))
            retStr = "";
        else
            retStr = idStr;
        return retStr;
    }

    public static String toCamelCase(String s) {
        boolean cap = true;
        char[] out = s.toCharArray();
        int i, len = s.length();
        for (i = 0; i < len; i++) {
            if (Character.isWhitespace(out[i])) {
                cap = true;
                continue;
            }
            if (cap) {
                out[i] = Character.toUpperCase(out[i]);
                cap = false;
            }
        }
        return new String(out);
    }

    public static String toHTML(String normalStr) {
        String htmlStr = normalStr.replaceAll("'", "&#39;").replaceAll("\"", "&quot;").replaceAll("<", "&lt;");
        return htmlStr;
    }

    public static String fromHTML(String htmlStr) {
        String normalStr = htmlStr.replaceAll("&#39;", "'").replaceAll("&quot;", "\"").replaceAll("&lt;", "<");
        return normalStr;
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static void setError(Context context, EditText edit, String str) {
        int resId = context.getResources().getIdentifier(str, "string", context.getPackageName());
        String msg = context.getResources().getString(resId);
        edit.setFocusableInTouchMode(true);
        edit.requestFocus();
        edit.setError(msg);
    }

    public static void setIDToast(Context context, String str) {
        int resId = getresIDStr(context, str);
        String msg = context.getResources().getString(resId);
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void setToast(Context context, String str) {
        Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
    }

    public static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isRequired(EditText editText) {
        if (editText.getText().toString().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isRequired(Context context,EditText editText) {
        if (editText.getText().toString().isEmpty()) {
            setError(context, editText, "required");
            return true;
        } else {
            editText.setError(null);
            return false;
        }
    }

    public static boolean isEmail(Context context, EditText editText) {
        if (isRequired(editText)) {
            setError(context, editText, "required");
            return true;
        } else if (!isRequired(editText) && !isValidEmail(editText.getText().toString())) {
            setError(context, editText, "err_email");
            return true;
        } else {
            editText.setError(null);
            return false;
        }
    }

    public static boolean isMobile(Context context, EditText editText) {
        if (isRequired(editText)) {
            setError(context, editText, "mand_mobile");
            return true;
        } else if (!isRequired(editText) && editText.getText().toString().length() < 10) {
            setError(context, editText, "err_mobile");
            return true;
        } else {
            editText.setError(null);
            return false;
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        boolean available = false;
        doCheck();
        /** Getting the system's connectivity service */
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        /** Getting active network interface  to get the network's status */
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isAvailable())
            available = true;

        /** Returning the status of the network */
        return available;
    }

    public static void doCheck() {
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    public static boolean DiffCurrent(String toDate){

        boolean isAfter = false;

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");

        String response = "";
        try {

            Date oldDate = dateFormat.parse(toDate);
            System.out.println("oldDate:"+oldDate);

            Date currentDate = new Date();

            long diff = currentDate.getTime() - oldDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (oldDate.after(currentDate)) {
                isAfter = true;
            } else {
                isAfter = false;
            }

            // Log.e("toyBornTime", "" + toyBornTime);

        } catch (ParseException e) {

            e.printStackTrace();
        }

        return isAfter;
    }
    public static boolean DiffEnable(String toDate){

        boolean isAfter = false;

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");

        String response = "";
        try {

            Date oldDate = dateFormat.parse(toDate);
            System.out.println("oldDate:"+oldDate);

            Date currentDate = new Date();

            long diff = currentDate.getTime() - oldDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;
            System.out.println("currentDate:"+currentDate);
            if (currentDate.after(oldDate) || currentDate.equals(oldDate)) {
                isAfter = true;
            } else {
                isAfter = false;
            }

            // Log.e("toyBornTime", "" + toyBornTime);

        } catch (ParseException e) {

            e.printStackTrace();
        }

        return isAfter;
    }

    public static long getCurrentDateLong(){
        Date currentDate = new Date();

        return currentDate.getTime();
    }

    public static int getPercentageLeft(String enddatestr) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        Date start = null;
        Date end = null;
        Date currentDate = null;

        int year = Calendar.getInstance().get(Calendar.YEAR);
        try {

            start = dateFormat.parse(year+"-01-01 00:00:00");
            end = dateFormat.parse(enddatestr);
            currentDate = new Date();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long s = start.getTime();
        long e = end.getTime();
        long now = currentDate.getTime();

        if (s >= e || now >= e) {
            return 0;
        }
        if (now <= s) {
            return 100;
        }
        return (int) (100 - ((e - now) * 100 / (e - s)));
    }
}

