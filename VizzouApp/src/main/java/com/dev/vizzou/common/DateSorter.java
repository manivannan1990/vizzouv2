package com.dev.vizzou.common;

/**
 * Created by karthikn on 4/20/16.
 */
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import data.episode_videos;

public class DateSorter implements Comparator {
    public int compare(Object firstObjToCompare, Object secondObjToCompare) {
        String firstDateString = ((episode_videos) firstObjToCompare).getCreated_on();
        String secondDateString = ((episode_videos) secondObjToCompare).getCreated_on();


        if (secondDateString == null || firstDateString == null) {
            return 0;
        }

        // Convert to Dates
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.0");
        DateTime firstDate = dtf.parseDateTime(firstDateString);
        DateTime secondDate = dtf.parseDateTime(secondDateString);

        if (firstDate.isAfter(secondDate)) return -1;
        else if (firstDate.isBefore(secondDate)) return 1;
        else return 0;
    }
}