package com.dev.vizzou.common;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import com.dev.vizzou.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import utils.camerautils.AndroidMultiPartEntity;

@SuppressWarnings("ALL")
@SuppressLint("NewApi")
public class BackgroundTask extends AsyncTask<String, String, String> {

    public static String respnse = "";
    public static String json = "test";
    public static String parameters = "";
    public static String action = "";
    long totalSize = 0;
    AppConstants appConstants = new AppConstants();

    private Context mContext;
    private View mView;
    ProgressDialog mDialog;
    String mflag = "";
    boolean mloadtext = false;

    public BackgroundTask(Context context) {
        mContext = context;
    }

    public BackgroundTask(Context context, String flag) {
        mContext = context;
        mflag = flag;
    }

    public BackgroundTask(Context context, String flag, boolean loadtext) {
        mContext = context;
        mflag = flag;
        mloadtext = loadtext;
    }

    protected void onPostExecute(String result) {
        Log.e("result", "result:" + result.length());
        if (result.equals("internet")) {
            if (mDialog.isShowing())
                mDialog.dismiss();
        } else {
            respnse = result;
            super.onPostExecute(result);
            ((OnTaskCompleted) mContext).onTaskCompleted(result, mflag);
            if (mDialog.isShowing())
                mDialog.dismiss();
        }
    }

    public Status statusReturn() {
        return this.getStatus();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog = new ProgressDialog(mContext, R.style.MyTheme);
        if (!mflag.equalsIgnoreCase("session")) {
            mDialog.setCancelable(false);
            mDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            mDialog.show();
        }
    }

    @SuppressWarnings("static-access")
    @Override
    protected String doInBackground(String... params) {
        if (AppConstants.isNetworkAvailable(mContext)) {
            action = params[0];
            parameters = params[1];
            Log.e("action", action);
            Log.e("parameters", parameters);
            String url;
            url = appConstants.BASE_URL + action;

            if(mflag.equalsIgnoreCase(appConstants.UPLOAD)){
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);

                try {
                    AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                            new AndroidMultiPartEntity.ProgressListener() {

                                @Override
                                public void transferred(long num) {
//                                    publishProgress((int) ((num / (float) totalSize) * 100));
                                }
                            });
                    JSONObject jsonParm = new JSONObject(parameters);

                    File sourceFile = new File(jsonParm.getString("videofile"));

                    JSONObject jsonParms = jsonParm.getJSONObject("parameters");

                    Bitmap bMap = ThumbnailUtils.createVideoThumbnail(jsonParm.getString("videofile"), MediaStore.Video.Thumbnails.MICRO_KIND);

                    Date d = new Date();
                    String timestamp = String.valueOf(d.getTime());
                    File thumbnail_file = new File(mContext.getCacheDir(), timestamp+".png");
                    thumbnail_file.createNewFile();

                    //Convert bitmap to byte array
                    Bitmap bitmap = bMap;
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                    byte[] bitmapdata = bos.toByteArray();

                    //write the bytes in file
                    FileOutputStream fos = new FileOutputStream(thumbnail_file);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();

                    // Adding file data to http body
                    entity.addPart("videofile", new FileBody(sourceFile));
                    entity.addPart("imagefile", new FileBody(thumbnail_file));

                    Iterator<String> subIter = jsonParms.keys();
                    while (subIter.hasNext()) {
                        String key = subIter.next();
                        String paraStr = jsonParms.getString(key);

                        entity.addPart(key, new StringBody(paraStr));
                    }

                    totalSize = entity.getContentLength();
                    httppost.setEntity(entity);

                    // Making server call
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity r_entity = response.getEntity();

                    int statusCode = response.getStatusLine().getStatusCode();
                    if (statusCode == 200) {
                        // Server response
                        json = EntityUtils.toString(r_entity);
                    } else {
                        json = "Error occurred! Http Status Code: "
                                + statusCode;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    json = e.toString();
                } catch (IOException e) {
                    json = e.toString();
                }

            }/*if(mflag.equalsIgnoreCase(appConstants.UPLOADTEMP)){
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);

                try {
                    AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                            new AndroidMultiPartEntity.ProgressListener() {

                                @Override
                                public void transferred(long num) {
//                                    publishProgress((int) ((num / (float) totalSize) * 100));
                                }
                            });
                    JSONObject jsonParm = new JSONObject(parameters);

                    File sourceFile = new File(jsonParm.getString("videofile"));
                    File sourceimageFile = new File(jsonParm.getString("imagefile"));

                    JSONObject jsonParms = jsonParm.getJSONObject("parameters");


                    // Adding file data to http body
                    entity.addPart("videofile", new FileBody(sourceFile));
                    entity.addPart("imagefile", new FileBody(sourceimageFile));

                    Iterator<String> subIter = jsonParms.keys();
                    while (subIter.hasNext()) {
                        String key = subIter.next();
                        String paraStr = jsonParms.getString(key);

                        entity.addPart(key, new StringBody(paraStr));
                    }

                    totalSize = entity.getContentLength();
                    httppost.setEntity(entity);

                    // Making server call
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity r_entity = response.getEntity();

                    int statusCode = response.getStatusLine().getStatusCode();
                    if (statusCode == 200) {
                        // Server response
                        json = EntityUtils.toString(r_entity);
                    } else {
                        json = "Error occurred! Http Status Code: "
                                + statusCode;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    json = e.toString();
                } catch (IOException e) {
                    json = e.toString();
                }

            }*/if(mflag.equalsIgnoreCase(appConstants.UPLOADIMAGE)){
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);

                try {
                    AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                            new AndroidMultiPartEntity.ProgressListener() {

                                @Override
                                public void transferred(long num) {
//                                    publishProgress((int) ((num / (float) totalSize) * 100));
                                }
                            });
                    JSONObject jsonParm = new JSONObject(parameters);

                    File sourceFile = new File(jsonParm.getString("videofile"));

                    JSONObject jsonParms = jsonParm.getJSONObject("parameters");

//
                    // Adding file data to http body
                    entity.addPart("videofile", new FileBody(sourceFile));
                    entity.addPart("imagefile", new FileBody(sourceFile));

                    Iterator<String> subIter = jsonParms.keys();
                    while (subIter.hasNext()) {
                        String key = subIter.next();
                        String paraStr = jsonParms.getString(key);

                        entity.addPart(key, new StringBody(paraStr));
                    }

                    totalSize = entity.getContentLength();
                    httppost.setEntity(entity);

                    // Making server call
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity r_entity = response.getEntity();

                    int statusCode = response.getStatusLine().getStatusCode();
                    if (statusCode == 200) {
                        // Server response
                        json = EntityUtils.toString(r_entity);
                    } else {
                        json = "Error occurred! Http Status Code: "
                                + statusCode;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    json = e.toString();
                } catch (IOException e) {
                    json = e.toString();
                }

            }else if(mflag.equalsIgnoreCase(appConstants.UPDATE_USER)){
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);

                try {
                    AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                            new AndroidMultiPartEntity.ProgressListener() {

                                @Override
                                public void transferred(long num) {
//                                    publishProgress((int) ((num / (float) totalSize) * 100));
                                }
                            });
                    JSONObject jsonParm = new JSONObject(parameters);

                    File sourceFile = new File(jsonParm.getString("profilepicture"));

                    JSONObject jsonParms = jsonParm.getJSONObject("parameters");


                    entity.addPart("profilepic", new FileBody(sourceFile));

                    Iterator<String> subIter = jsonParms.keys();
                    while (subIter.hasNext()) {
                        String key = subIter.next();
                        String paraStr = jsonParms.getString(key);

                        entity.addPart(key, new StringBody(paraStr));
                    }

                    totalSize = entity.getContentLength();
                    httppost.setEntity(entity);

                    // Making server call
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity r_entity = response.getEntity();

                    int statusCode = response.getStatusLine().getStatusCode();
                    if (statusCode == 200) {
                        // Server response
                        json = EntityUtils.toString(r_entity);
                    } else {
                        json = "Error occurred! Http Status Code: "
                                + statusCode;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    json = e.toString();
                } catch (IOException e) {
                    json = e.toString();
                }
            }else {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);

                try {
                    AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                            new AndroidMultiPartEntity.ProgressListener() {

                                @Override
                                public void transferred(long num) {
                                }
                            });
                    JSONObject jsonParm = new JSONObject(parameters);

                    Iterator<String> subIter = jsonParm.keys();
                    while (subIter.hasNext()) {
                        String key = subIter.next();
                        String paraStr = jsonParm.getString(key);

                        entity.addPart(key, new StringBody(paraStr));
                    }

                    totalSize = entity.getContentLength();
                    httppost.setEntity(entity);

                    // Making server call
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity r_entity = response.getEntity();

                    int statusCode = response.getStatusLine().getStatusCode();
                    if (statusCode == 200) {
                        // Server response
                        json = EntityUtils.toString(r_entity);
                    } else {
                        json = "Error occurred! Http Status Code: "
                                + statusCode;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    json = e.toString();
                } catch (IOException e) {
                    json = e.toString();
                }
            }


            return json;
        } else {
            return "internet";
        }
    }

    @SuppressLint("CutPasteId")
    protected void handleResponse(String action, String Response) throws IOException {
        if (mDialog.isShowing())
            mDialog.dismiss();
    }


}


