package com.dev.vizzou.common;

public interface OnTaskCompleted {
    public void onTaskCompleted(String values, String flag);
}