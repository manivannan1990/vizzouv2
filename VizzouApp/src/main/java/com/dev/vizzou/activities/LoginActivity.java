package com.dev.vizzou.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.vizzou.R;
import com.dev.vizzou.common.AppConstants;
import com.dev.vizzou.common.BackgroundTask;
import com.dev.vizzou.common.OnTaskCompleted;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import butterknife.InjectView;
import connection.AppConfig;

/**
 * Created by ceino on 21/3/16.
 */
@SuppressWarnings("deprecation")
public class LoginActivity extends Activity implements View.OnClickListener, OnTaskCompleted {


    private static final String TAG = "LoginActivity";
    Context context = this;

    AppConstants appConstants = new AppConstants();

    int flag =0;
    SharedPreferences sharedpreferences;

    @InjectView(R.id.account_txv)LinearLayout register;
    @InjectView(R.id.signin_txv)LinearLayout signin;

    @InjectView(R.id.usrname_edtxt)EditText usrname_edtxt;
    @InjectView(R.id.email_edtxt)EditText email_edtxt;
    @InjectView(R.id.pwd_edt)EditText pwd_edtxt;

    @InjectView(R.id.submit_signin)TextView submit_signin;
    @InjectView(R.id.submit_register)TextView submit_register;

    @InjectView(R.id.spinner_layout)LinearLayout spinner_layout;
    @InjectView(R.id.spinner)ImageView spinner;

    JSONObject params;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);

        ((Animatable) spinner.getDrawable()).start();

        submit_signin.setOnClickListener(this);
        submit_register.setOnClickListener(this);


        register.setOnClickListener(this);
        signin.setOnClickListener(this);


    }


    @Override
    protected void onDestroy() {

        super.onDestroy();

        ButterKnife.reset(this);
    }

    @SuppressLint("NewApi")
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.signin_txv:
                usrname_edtxt.setVisibility(View.GONE);
                email_edtxt.setBackground(getResources().getDrawable(R.drawable.square_white_border_top));
                pwd_edtxt.setBackground(getResources().getDrawable(R.drawable.square_white_border_bottom));
                submit_signin.setVisibility(View.VISIBLE);
                submit_register.setVisibility(View.GONE);
                usrname_edtxt.setError(null);
                email_edtxt.setError(null);
                pwd_edtxt.setError(null);
                register.setVisibility(View.VISIBLE);
                signin.setVisibility(View.INVISIBLE);
                break;

            case R.id.account_txv:
                usrname_edtxt.setVisibility(View.VISIBLE);
                usrname_edtxt.setBackground(getResources().getDrawable(R.drawable.square_white_border_top));
                email_edtxt.setBackground(getResources().getDrawable(R.drawable.square_white));
                pwd_edtxt.setBackground(getResources().getDrawable(R.drawable.square_white_border_bottom));
                submit_signin.setVisibility(View.GONE);
                submit_register.setVisibility(View.VISIBLE);
                usrname_edtxt.setError(null);
                email_edtxt.setError(null);
                pwd_edtxt.setError(null);
                register.setVisibility(View.INVISIBLE);
                signin.setVisibility(View.VISIBLE);
                break;


            case R.id.submit_signin:
                if(isNull(email_edtxt)){
                    email_edtxt.requestFocus();
                    email_edtxt.setError("Enter email");
                }else if(isNull(pwd_edtxt)){
                    pwd_edtxt.requestFocus();
                    pwd_edtxt.setError("Enter password");
                }else {
                    doRequest(appConstants.LOGIN);
                }

                break;
            case R.id.submit_register:
                if(isNull(usrname_edtxt)){
                    usrname_edtxt.requestFocus();
                    usrname_edtxt.setError("Enter username");
                }else if(isNull(email_edtxt)){
                    email_edtxt.requestFocus();
                    email_edtxt.setError("Enter e-mail");
                }else if(checkValidEmail(email_edtxt.getText().toString()).equalsIgnoreCase("invalid email") ) {
                    email_edtxt.requestFocus();
                    email_edtxt.setError("Enter valid email");
                }else if(isNullA(pwd_edtxt)){
                    pwd_edtxt.requestFocus();
                    if(flag == 1)
                    pwd_edtxt.setError("Enter password");
                    if(flag == 2)
                        pwd_edtxt.setError("Password at least 6 characters");
                }else {
                    doRequest(appConstants.REGISTER);
                    usrname_edtxt.getText().clear();
                    email_edtxt.getText().clear();
                    pwd_edtxt.getText().clear();
                }
                break;
        }
    }
    private void doRequest(String tag){
        try {
            params = new JSONObject();
            if(tag.equalsIgnoreCase(appConstants.LOGIN)){
                params.put("email", email_edtxt.getText().toString());
                params.put("password", pwd_edtxt.getText().toString());
            }
            if(tag.equalsIgnoreCase(appConstants.REGISTER)){
                params.put("firstname", usrname_edtxt.getText().toString());
                params.put("lastname", "");
                params.put("email", email_edtxt.getText().toString());
                params.put("password", pwd_edtxt.getText().toString());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        BackgroundTask bt = new BackgroundTask(context,tag);
        if(tag.equalsIgnoreCase(appConstants.LOGIN)){
            bt.execute(AppConstants.ACTION_LOGIN, params.toString());
        }
        if(tag.equalsIgnoreCase(appConstants.REGISTER)){
            bt.execute(AppConstants.ACTION_REGISTER, params.toString());
        }

    }

    @Override
    public void onTaskCompleted(String values, String flag) {

        if(flag.equalsIgnoreCase(appConstants.LOGIN)){
            Log.e("Response :login:", values);
            try {
                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")) {
                    if (jsobj.getString("success").equalsIgnoreCase("true")) {
                        JSONArray resjsobj = jsobj.getJSONArray("result");
                        JSONObject obj = resjsobj.getJSONObject(0);
                        Log.e("MyResponse :login:", obj.toString());
                        String accessId = obj.getString("uuid");
                        String accessEmail = obj.getString("email");
                        String firstName = obj.getString("firstname");
                        String pwd = obj.getString("password");
                        String image = obj.getString("profilepicture");
                        if(obj.has("longtiutde") || obj.has("lattitude")){}
                        sharedpreferences = getSharedPreferences(AppConfig.MYPREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(AppConfig.EMAIL, accessEmail);
                        editor.putString(AppConfig.ID, accessId);
                        editor.putString(AppConfig.NAME, firstName);
                        editor.commit();
//                        vizzouApplication.saveVizzouAccessToken(accessId, accessEmail, firstName, pwd,image);
                        Toast.makeText(this, "Login Success", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MainTabActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        LoginActivity.this.finish();
                        startActivity(intent);
                    }else {
                        Log.e("MyResponse:","fails");
                        Toast.makeText(this,jsobj.getString("result").toString(), Toast.LENGTH_SHORT).show();
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(flag.equalsIgnoreCase(appConstants.REGISTER)) {
            try {
                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")) {
                    if (jsobj.getString("success").equalsIgnoreCase("true")) {
                        Log.e("MyResponse :register:", jsobj.toString());
                        JSONArray resjsobj = jsobj.getJSONArray("result");
                        JSONObject obj = resjsobj.getJSONObject(0);
                        String accessId = obj.getString("uuid");
                        String accessEmail = obj.getString("email");
                        String firstName = obj.getString("firstname");
                        if(obj.has("longtiutde") || obj.has("lattitude")){}
                        sharedpreferences = getSharedPreferences(AppConfig.MYPREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(AppConfig.EMAIL, accessEmail);
                        editor.putString(AppConfig.ID, accessId);
                        editor.putString(AppConfig.NAME, firstName);
                        editor.commit();

                        Toast.makeText(LoginActivity.this, "Register Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MainTabActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        LoginActivity.this.finish();
                        startActivity(intent);
                    }else {
                        Log.e("MyResponse:","fails");
                        Toast.makeText(LoginActivity.this, jsobj.getString("result").toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    Boolean isNull(EditText editText)
    {
        if(editText!=null)
        {
            if(editText.getText()==null ||editText.getText().toString().trim().equalsIgnoreCase(""))
            {
                return true;
            }
            return false;
        }
        return true;
    }
    Boolean isNullA(EditText editText)
    {
        if(editText!=null)
        {
            if(editText.getText()==null ||editText.getText().toString().trim().equalsIgnoreCase("")){
                flag =1;
                return true;
            }
            else if(editText.getText().toString().length() <= 6){
                flag =2;
                return true;
            }
            return false;
        }
        return true;
    }
    public String checkValidEmail(String email){
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if(matcher.matches()){
            return "valid email";
        }
        else
            return "invalid email";
    }
}
