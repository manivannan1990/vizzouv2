package com.dev.vizzou.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dev.vizzou.R;
import com.dev.vizzou.common.AppConstants;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by ceino on 9/3/16.
 */
public class AddEpisodeActivity extends Activity implements View.OnClickListener {


    private static final String TAG = "AddEpisodeActivity";

    @InjectView(R.id.close_page)LinearLayout closePage;
    @InjectView(R.id.submit)ImageView submit;
    @InjectView(R.id.NewEpisode)RelativeLayout NewEpisode;
    @InjectView(R.id.NewEpisodeFriends)RelativeLayout NewEpisodeFriends;
    @InjectView(R.id.NewEpisodeOpen)RelativeLayout NewEpisodeOpen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addepisode);
        ButterKnife.inject(this);
        NewEpisode.setOnClickListener(this);
        NewEpisodeFriends.setOnClickListener(this);
        NewEpisodeOpen.setOnClickListener(this);
        closePage.setOnClickListener(this);
        submit.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

        ButterKnife.reset(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.close_page:
                finish();
                break;
            case R.id.submit:
                Toast.makeText(this, "Under Construction...", Toast.LENGTH_SHORT).show();
                break;
            case R.id.NewEpisode:
                Intent intent1 = new Intent(this, CreateNewEpisode.class);
                intent1.putExtra(AppConstants.intent_filePath, getIntent().getStringExtra(AppConstants.intent_filePath));
                intent1.putExtra(AppConstants.user_id, getIntent().getStringExtra(AppConstants.user_id));
                intent1.putExtra(AppConstants.is_Camera,getIntent().getBooleanExtra(AppConstants.is_Camera,false));
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent1);
                break;
            case R.id.NewEpisodeFriends:
                Intent intent2 = new Intent(this, CreateNewEpisodeFriends.class);
                intent2.putExtra(AppConstants.intent_filePath, getIntent().getStringExtra(AppConstants.intent_filePath));
                intent2.putExtra(AppConstants.user_id, getIntent().getStringExtra(AppConstants.user_id));
                intent2.putExtra(AppConstants.is_Camera,getIntent().getBooleanExtra(AppConstants.is_Camera, false));
                intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent2);
                break;
            case R.id.NewEpisodeOpen:
                Intent intent3 = new Intent(this, CreateNewEpisodeOpen.class);
                intent3.putExtra(AppConstants.intent_filePath, getIntent().getStringExtra(AppConstants.intent_filePath));
                intent3.putExtra(AppConstants.user_id, getIntent().getStringExtra(AppConstants.user_id));
                intent3.putExtra(AppConstants.is_Camera,getIntent().getBooleanExtra(AppConstants.is_Camera, false));
                intent3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent3);
                break;
        }
    }
}
