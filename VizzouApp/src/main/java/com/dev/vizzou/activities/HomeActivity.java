package com.dev.vizzou.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.vizzou.R;
import com.dev.vizzou.common.AppConstants;
import com.dev.vizzou.common.BackgroundTask;
import com.dev.vizzou.common.DateSorter;
import com.dev.vizzou.common.OnTaskCompleted;
import com.dev.vizzou.videostream.VideoPlayerActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.ButterKnife;
import butterknife.InjectView;
import connection.AppConfig;
import data.episode_videos;
import utils.VizzouBus;

/**
 * Created by ceino on 23/3/16.
 */
public class HomeActivity extends Activity implements View.OnClickListener, OnTaskCompleted {

    private static final String TAG = "HomeActivity";

    Context context = this;
    SharedPreferences sharedpreferences;

    @InjectView(R.id.videoicon)ImageView videoIcon;
    @InjectView(R.id.layout1)LinearLayout layout1;
    @InjectView(R.id.layout2)LinearLayout layout2;
    @InjectView(R.id.layout3)LinearLayout layout3;
    @InjectView(R.id.layout4)LinearLayout layout4;
    @InjectView(R.id.recycler_view1)utils.CustomRecyclerView recyclerView1;
    @InjectView(R.id.recycler_view2)utils.CustomRecyclerView recyclerView2;
    @InjectView(R.id.recycler_view3)utils.CustomRecyclerView recyclerView3;
    @InjectView(R.id.recycler_view4)utils.CustomRecyclerView recyclerView4;

    @InjectView(R.id.novideos_1)TextView novideos_1;
    @InjectView(R.id.novideos_2)TextView novideos_2;
    @InjectView(R.id.novideos_3)TextView novideos_3;
    @InjectView(R.id.novideos_4)TextView novideos_4;

    @InjectView(R.id.addwatchnowframe)RelativeLayout addToWatchNowFrame;
    @InjectView(R.id.addtowatchbtn)TextView addWatchBtn;
    @InjectView(R.id.removefromwatchbtn)TextView removeWatchBtn;
    @InjectView(R.id.cancelbtn)TextView cancelBtn;


    RecyclerView_Adapter recyclerView_adapter, recyclerView_adapter_friends, recyclerView_adapter_open, recyclerView_adapter_watchnow;
//    RecyclerView_Adapter_watchnow recyclerView_adapter_watchnow;
    JSONObject params;

    ArrayList<episode_videos> myepisodesList;
    ArrayList<episode_videos> myepisodesFriendsList;
    ArrayList<episode_videos> watchnowList;
    ArrayList<episode_videos> myepisodesOpenList;
    String userid;

    boolean firstClick1 = false,firstClick2 = false,firstClick3 = false,firstClick4 = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.inject(this);
        VizzouBus.getInstance().register(this);

        sharedpreferences = this.getSharedPreferences(AppConfig.MYPREFERENCES, Context.MODE_PRIVATE);
        userid = sharedpreferences.getString(AppConfig.ID, "");

        layout1.setOnClickListener(this);
        layout2.setOnClickListener(this);
        layout3.setOnClickListener(this);
        layout4.setOnClickListener(this);
        videoIcon.setOnClickListener(this);
        addToWatchNowFrame.setOnClickListener(null);
        addWatchBtn.setOnClickListener(this);
        removeWatchBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);

        recyclerView1.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView2.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView3.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView4.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));



        doRequest(AppConstants.GETEPISODES);
        doRequest(AppConstants.GETEPISODESFRIENDS);
        doRequest(AppConstants.GETEPISODESOPEN);

    }

    private void doRequest(String tag){

        try {
            params = new JSONObject();
            if(tag.equalsIgnoreCase(AppConstants.GETEPISODES)){
                params.put("limit", "0");
                params.put("offset", "0");
                params.put("tag", tag);
                params.put("userid", userid);
//            }else if(tag.equalsIgnoreCase(AppConstants.GETEPISODESOPEN) || tag.equalsIgnoreCase(AppConstants.GETEPISODESFRIENDS)){
            }else {
                params.put("Limit", "0");
                params.put("Offset", "0");
                params.put("userid", userid);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        BackgroundTask bt = new BackgroundTask(context,tag);
        if(tag.equalsIgnoreCase(AppConstants.GETEPISODES)){
            bt.execute(AppConstants.ACTION_GETEPISODES, params.toString());
        }else if(tag.equalsIgnoreCase(AppConstants.GETEPISODESFRIENDS)){
            bt.execute(AppConstants.ACTION_GETEPISODESFRIENDS, params.toString());
        }else if(tag.equalsIgnoreCase(AppConstants.GETEPISODESOPEN)){
            bt.execute(AppConstants.ACTION_GETEPISODESOPEN, params.toString());
        }else if(tag.equalsIgnoreCase(AppConstants.GETWATCHNOW)){
            bt.execute(AppConstants.ACTION_WATCHNOW, params.toString());
        }
    }

    private void doAddWatchNowRequest(String episodeid, String friendid, String flag, String usrId, String feid){
        try {
            params = new JSONObject();
            params.put("friendid", friendid);
            params.put("episodeid", episodeid);
            params.put("userid", usrId);
            params.put("feid", feid);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        BackgroundTask bt = new BackgroundTask(context,flag);
        bt.execute(AppConstants.ACTION_ADDWATCHNOW, params.toString());
    }
    private void doUpdateWatchNowRequest(String feid, String flag){
        try {
            params = new JSONObject();
            params.put("feid", feid);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        BackgroundTask bt = new BackgroundTask(context,flag);
        bt.execute(AppConstants.ACTION_UPDATEWATCHNOW, params.toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        doRequest(AppConstants.GETWATCHNOW);
        doRequest(AppConstants.GETEPISODESFRIENDS);
        doRequest(AppConstants.GETEPISODESOPEN);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(addToWatchNowFrame.getVisibility() == View.VISIBLE){
            addToWatchNowFrame.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if(addToWatchNowFrame.getVisibility() == View.VISIBLE){
            addToWatchNowFrame.setVisibility(View.GONE);
        }else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
        VizzouBus.getInstance().unregister(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout1:
                if(firstClick1){
                    recyclerView1.setVisibility(View.VISIBLE);
                    firstClick1 = false;
                }else {
                    recyclerView1.setVisibility(View.GONE);
                    firstClick1 = true;
                }
                break;
            case R.id.layout2:
                if(firstClick2){
                    recyclerView2.setVisibility(View.VISIBLE);
                    firstClick2 = false;
                }else {
                    recyclerView2.setVisibility(View.GONE);
                    firstClick2 = true;
                }
                break;
            case R.id.layout3:
                if(firstClick3){
                    recyclerView3.setVisibility(View.VISIBLE);
                    firstClick3 = false;
                }else {
                    recyclerView3.setVisibility(View.GONE);
                    firstClick3 = true;
                }
                break;
            case R.id.layout4:
                if(firstClick4){
                    recyclerView4.setVisibility(View.VISIBLE);
                    firstClick4 = false;
                }else {
                    recyclerView4.setVisibility(View.GONE);
                    firstClick4 = true;
                }
                break;
            case R.id.videoicon:
                captureVideo();
                break;
            case R.id.addtowatchbtn:
//                Toast.makeText(HomeActivity.this, "Service constructed", Toast.LENGTH_SHORT).show();
//                System.out.println("datas:"+"epiId:"+AppConstants.EPISODE_ID +"FId:"+AppConstants.FRIEND_ID +"flag:"+AppConstants.FLAG);
                doAddWatchNowRequest(AppConstants.EPISODE_ID, AppConstants.FRIEND_ID,AppConstants.FLAG,AppConstants.USR_ID,AppConstants.FE_ID);
                break;
            case R.id.removefromwatchbtn:
//                Toast.makeText(HomeActivity.this, "AppConstants.FE_ID:"+AppConstants.FE_ID, Toast.LENGTH_SHORT).show();
                doUpdateWatchNowRequest(AppConstants.FE_ID,AppConstants.FLAG);
                break;
            case R.id.cancelbtn:
                addToWatchNowFrame.setVisibility(View.GONE);
                break;
        }
    }


    private void recordVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

//        fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);

        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 60);
        // set video quality
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

//        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file
        // name

        // start the video capture Intent
        startActivityForResult(intent, 1111);
    }


    @Override
    public void onTaskCompleted(String values, String flag) {
        Log.e("ResponseErrrrror",values);
        myepisodesList = new ArrayList<episode_videos>();
        myepisodesFriendsList = new ArrayList<episode_videos>();
        myepisodesOpenList = new ArrayList<episode_videos>();
        watchnowList = new ArrayList<episode_videos>();
        episode_videos episode_item;
        try {
            if(flag.equalsIgnoreCase(AppConstants.GETEPISODES)) {

                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")){
                    if(jsobj.getString("success").equalsIgnoreCase("true")){
                        JSONArray jsarr = jsobj.getJSONArray("result");
                        Log.e("MyResponse1234",jsarr.toString());
                        for(int i=0;i<jsarr.length();i++){
                            JSONObject obj = jsarr.getJSONObject(i);
                            Log.e("MyResponse:"+i+" :",obj.toString());

                            episode_item = new episode_videos();

                            episode_item.setUserid(obj.getString("userid"));
                            episode_item.setImagename(obj.getString("imagename"));
                            episode_item.setTotalviewcount(obj.getString("totalviewcount"));
                            episode_item.setVideoname(obj.getString("videoname"));
                            episode_item.setEpisodename(obj.getString("episodename"));
                            episode_item.setCreated_on(obj.getString("created_on"));
                            episode_item.setEpisodeid(obj.getString("episodeid"));
                            episode_item.setImagename(obj.getString("imagename"));
                            episode_item.setFirstname(obj.getString("firstname"));
                            episode_item.setReleasedate(obj.getString("releasedate"));
                            episode_item.setKeywords(obj.getString("keywords"));
                            episode_item.setisTVicon("1");

                            if(AppConstants.DiffCurrent(obj.getString("releasedate")))
                                myepisodesList.add(episode_item);

                        }

                    }else {
                        Log.e("MyResponse:","fails");
                    }
                }

                Collections.sort(myepisodesList, new DateSorter());
                if(myepisodesList.size()>0) {
                    recyclerView1.setVisibility(View.VISIBLE);
                    novideos_1.setVisibility(View.GONE);

                    recyclerView_adapter = new RecyclerView_Adapter(this, myepisodesList,userid,flag);
                    recyclerView1.setAdapter(recyclerView_adapter);
                }else{
                    recyclerView1.setVisibility(View.GONE);
                    novideos_1.setVisibility(View.VISIBLE);
                }
            }else if(flag.equalsIgnoreCase(AppConstants.GETEPISODESFRIENDS)) {

                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")){
                    if(jsobj.getString("success").equalsIgnoreCase("true")){
                        JSONArray jsarr = jsobj.getJSONArray("result");
                        for(int i=0;i<jsarr.length();i++){
                            JSONObject obj = jsarr.getJSONObject(i);
                            Log.e("MyResponse:"+i+" :",obj.toString());

                            episode_item = new episode_videos();

                            episode_item.setUserid(obj.getString("userid"));
                            episode_item.setImagename(obj.getString("imagename"));
                            episode_item.setTotalviewcount(obj.getString("totalviewcount"));
                            episode_item.setVideoname(obj.getString("videoname"));
                            episode_item.setEpisodename(obj.getString("episodename"));
                            episode_item.setCreated_on(obj.getString("created_on"));
                            episode_item.setEpisodeid(obj.getString("episodeid"));
                            episode_item.setImagename(obj.getString("imagename"));
                            episode_item.setFirstname(obj.getString("firstname"));
                            episode_item.setFriendid(obj.getString("friendid"));
                            episode_item.setReleasedate(obj.getString("releasedate"));
                            episode_item.setKeywords(obj.getString("keywords"));
                            episode_item.setisTVicon("2");
                            episode_item.setWatched(obj.getBoolean("watched"));
                            episode_item.setFeid(obj.getString("feid"));

                            if(AppConstants.DiffCurrent(obj.getString("releasedate")))
                                myepisodesFriendsList.add(episode_item);
                        }
                    }else {
                        Log.e("MyResponse:","fails");
                    }
                }
                Collections.sort(myepisodesFriendsList, new DateSorter());
                if(myepisodesFriendsList.size()>0) {
                    recyclerView2.setVisibility(View.VISIBLE);
                    novideos_2.setVisibility(View.GONE);

                    recyclerView_adapter_friends = new RecyclerView_Adapter(this, myepisodesFriendsList,userid,flag);
                    recyclerView2.setAdapter(recyclerView_adapter_friends);
                }else{
                    recyclerView2.setVisibility(View.GONE);
                    novideos_2.setVisibility(View.VISIBLE);
                }

            }else if(flag.equalsIgnoreCase(AppConstants.GETWATCHNOW)) {

                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")){
                    if(jsobj.getString("success").equalsIgnoreCase("true")){
                        JSONArray jsarr = jsobj.getJSONArray("result");
                        for(int i=0;i<jsarr.length();i++){
                            JSONObject obj = jsarr.getJSONObject(i);
                            Log.e("MyResponse:"+i+" :",obj.toString());

                            episode_item = new episode_videos();

                            episode_item.setUserid(obj.getString("userid"));
                            episode_item.setImagename(obj.getString("imagename"));
                            episode_item.setTotalviewcount(obj.getString("totalviewcount"));
                            episode_item.setVideoname(obj.getString("videoname"));
                            episode_item.setEpisodename(obj.getString("episodename"));
                            episode_item.setEpisodeid(obj.getString("episodeid"));
                            episode_item.setFirstname(obj.getString("firstname"));
                            episode_item.setReleasedate(obj.getString("releasedate"));
                            episode_item.setKeywords(obj.getString("keywords"));
                            episode_item.setisTVicon("3");
                            episode_item.setType(obj.getString("type"));
                            episode_item.setFeid(obj.getString("feid"));
                            episode_item.setReferredid(obj.getString("referredfeid"));

//                            if(AppConstants.DiffCurrent(obj.getString("releasedate")))
                            watchnowList.add(episode_item);
                        }
                    }else {
                        Log.e("MyResponse:","fails");
                    }
                }


                if(watchnowList.size()>0) {
                    recyclerView3.setVisibility(View.VISIBLE);
                    novideos_3.setVisibility(View.GONE);

                    recyclerView_adapter_watchnow = new RecyclerView_Adapter(this, watchnowList,userid,flag);
                    recyclerView3.setAdapter(recyclerView_adapter_watchnow);
                }else{
                    recyclerView3.setVisibility(View.GONE);
                    novideos_3.setVisibility(View.VISIBLE);
                }

            }else if(flag.equalsIgnoreCase(AppConstants.GETEPISODESOPEN)) {

                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")){
                    if(jsobj.getString("success").equalsIgnoreCase("true")){
                        JSONArray jsarr = jsobj.getJSONArray("result");
                        for(int i=0;i<jsarr.length();i++){
                            JSONObject obj = jsarr.getJSONObject(i);
                            Log.e("MyResponse:"+i+" :",obj.toString());

                            episode_item = new episode_videos();

                            episode_item.setUserid(obj.getString("userid"));
                            episode_item.setCreated_by(obj.getString("created_by"));
                            episode_item.setImagename(obj.getString("imagename"));
                            episode_item.setTotalviewcount(obj.getString("totalviewcount"));
                            episode_item.setVideoname(obj.getString("videoname"));
                            episode_item.setEpisodename(obj.getString("episodename"));
                            episode_item.setEpisodeid(obj.getString("episodeid"));
                            episode_item.setFirstname(obj.getString("firstname"));
                            episode_item.setReleasedate(obj.getString("releasedate"));
                            episode_item.setKeywords(obj.getString("keywords"));
                            episode_item.setisTVicon("0");
                            episode_item.setTag(obj.getString("tag"));
                            episode_item.setType(obj.getString("type"));
                            episode_item.setFeid(obj.getString("feid"));

                            myepisodesOpenList.add(episode_item);
                        }
                    }else {
                        Log.e("MyResponse:","fails");
                    }
                }


                if(myepisodesOpenList.size()>0) {
                    recyclerView4.setVisibility(View.VISIBLE);
                    novideos_4.setVisibility(View.GONE);

                    recyclerView_adapter_open = new RecyclerView_Adapter(this, myepisodesOpenList,userid,flag);
                    recyclerView4.setAdapter(recyclerView_adapter_open);
                }else{
                    recyclerView4.setVisibility(View.GONE);
                    novideos_4.setVisibility(View.VISIBLE);
                }

            }else if(flag.equalsIgnoreCase(AppConstants.FLAG)){
                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")){
                    if(jsobj.getString("success").equalsIgnoreCase("true")){
                        addToWatchNowFrame.setVisibility(View.GONE);
                        doRequest(AppConstants.GETWATCHNOW);
                        doRequest(AppConstants.GETEPISODESFRIENDS);
                        Toast.makeText(HomeActivity.this, "Video Added to Watch Now", Toast.LENGTH_SHORT).show();
                    }else {
                        Log.e("MyResponse:","fails");
                    }
                }
            }else if(flag.equalsIgnoreCase(AppConstants.FLAG)){
                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")){
                    if(jsobj.getString("success").equalsIgnoreCase("true")){
                        addToWatchNowFrame.setVisibility(View.GONE);
                        doRequest(AppConstants.GETWATCHNOW);
                        Toast.makeText(HomeActivity.this, "Video Removed from Watch Now", Toast.LENGTH_SHORT).show();
                    }else {
                        Log.e("MyResponse:","fails");
                    }
                }
            }else {
                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")){
                    if(jsobj.getString("success").equalsIgnoreCase("true")){
                        Intent intent = new Intent(context, VideoPlayerActivity.class);
                        intent.putExtra("URL", flag);
                        ((Activity) context).startActivity(intent);
                    }else {
                        Log.e("MyResponse:","fails");
                    }
                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void captureVideo(){

        Intent i = new Intent(this, CameraActivity.class);
        i.putExtra(AppConstants.user_id, userid);
        startActivity(i);
    }

    //Adapter
    public class RecyclerView_Adapter extends
            RecyclerView.Adapter<RecyclerView_Adapter.RecyclerViewHolder> {

        private ArrayList<episode_videos> arrayList;
        private Context context;
        String flag;
        String userid;

        public RecyclerView_Adapter(Context context, ArrayList<episode_videos> arrayList, String userid, String flag) {
            this.context = context;
            this.userid = userid;
            this.flag = flag;
            this.arrayList = arrayList;

        }

        @Override
        public int getItemCount() {
            return (null != arrayList ? arrayList.size() : 0);

        }

        @Override
        public void onBindViewHolder(RecyclerViewHolder holder, int position) {
            final episode_videos model = arrayList.get(position);

            final RecyclerViewHolder mainHolder = (RecyclerViewHolder) holder;// holder


            // setting title
            mainHolder.title.setText(model.getEpisodename());
            mainHolder.username.setText(model.getFirstname());
            /*if (model.getisTVicon().equalsIgnoreCase("1")) {
                mainHolder.tvicon.setVisibility(View.GONE);
            }*/

            mainHolder.tvicon.setVisibility(View.GONE);
            Picasso.with(context).load(AppConstants.IMAGE_URL + model.getImagename())
                    .error(R.drawable.placeholder)
                    .placeholder(R.drawable.placeholder)
                    .into(mainHolder.imageview);

            //Seekbar
            mainHolder.timeline.setProgress(AppConstants.getPercentageLeft(model.getReleasedate()));

            mainHolder.episode_item_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (model.getisTVicon().equalsIgnoreCase("1")) {
                        Intent intent = new Intent(context, EditOpenEpisodeFriends.class);
                        intent.putExtra("MyClass", model);
                        intent.putExtra(AppConstants.user_id, userid);
                        if(model.getVideoname().contains(".jpg")){
                            intent.putExtra(AppConstants.is_Camera,true);
                        }else {
                            intent.putExtra(AppConstants.is_Camera,false);
                        }

                        ((Activity) context).startActivity(intent);

                    } else if(model.getisTVicon().equalsIgnoreCase("2")){
                        addToWatchNowFrame.setVisibility(View.VISIBLE);
                        boolean isWatched = model.isWatched();
                        if(!isWatched){
                            addWatchBtn.setVisibility(View.VISIBLE);
                            removeWatchBtn.setVisibility(View.GONE);
                            AppConstants.FLAG = "ADDWATCHNOW";
                        }else {
                            removeWatchBtn.setVisibility(View.VISIBLE);
                            addWatchBtn.setVisibility(View.GONE);
                            AppConstants.FLAG = "UPDATEWATCHNOW";
                        }
                        String url = AppConstants.VIDEO_URL + model.getVideoname();
                        AppConstants.EPISODE_ID = model.getEpisodeid();
                        AppConstants.FRIEND_ID = model.getFriendid();
                        AppConstants.USR_ID = model.getUserid();
                        AppConstants.FE_ID = model.getFeid();

                    }else if(model.getisTVicon().equalsIgnoreCase("3")){
                        String url = AppConstants.VIDEO_URL + model.getVideoname();

                        Intent intent = new Intent(context, VideoPlayerActivity.class);
                        intent.putExtra("URL", url);
                        AppConstants.TYPE = model.getType();
                        AppConstants.FE_ID = model.getFeid();
                        AppConstants.REFERRED_ID = model.getReferredid();

                        if(AppConstants.DiffEnable(model.getReleasedate())){
                            ((Activity) context).startActivity(intent);
                        }
                        else
                            Toast.makeText(context, "Waiting for the release Time", Toast.LENGTH_SHORT).show();
                    }else {

                        Log.e("flag",flag);
                        /*if (flag.equalsIgnoreCase(AppConstants.GETEPISODESFRIENDS)) {
                            String url = AppConstants.VIDEO_URL + model.getVideoname();
                            doWatchNowRequest(model.getEpisodeid(), model.getFriendid(),url);
                        }else{
                            String url = AppConstants.VIDEO_URL + model.getVideoname();

                            Intent intent = new Intent(context, VideoPlayerActivity.class);
                            intent.putExtra("URL", url);
                            ((Activity) context).startActivity(intent);
                        }*/
                        String url = AppConstants.VIDEO_URL + model.getVideoname();

                        Intent intent = new Intent(context, VideoPlayerActivity.class);
                        intent.putExtra("URL", url);
                        ((Activity) context).startActivity(intent);
                        AppConstants.FE_ID = model.getFeid();
                        if(model.getCreated_by() != null){
                            String createdby = model.getCreated_by().toString();
                            if(createdby.equalsIgnoreCase(userid) ){
                                AppConstants.TYPE = "USELESS";
                            }else {
                                AppConstants.TYPE = "UNJOINOPEN";
                            }
                        }else {
                            AppConstants.TYPE = "USELESS";
                        }

                    }

                }
            });


        }

        @Override
        public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

            LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

            ViewGroup mainGroup = (ViewGroup) mInflater.inflate(
                    R.layout.item_row, viewGroup, false);
            RecyclerViewHolder listHolder = new RecyclerViewHolder(mainGroup);
            return listHolder;

        }

        public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            public TextView title, username;
            public utils.ProportionalImageView imageview;
            public ImageView tvicon;
            public SeekBar timeline;
            //        public View timeline;
            public LinearLayout episode_item_view;

            public RecyclerViewHolder(View view) {
                super(view);

                this.title = (TextView) view
                        .findViewById(R.id.title);
                this.username = (TextView) view
                        .findViewById(R.id.username);
                this.imageview = (utils.ProportionalImageView) view
                        .findViewById(R.id.image);
                this.tvicon = (ImageView) view.findViewById(R.id.tvicon);
                this.timeline = (SeekBar) view.findViewById(R.id.timeline);
                this.episode_item_view = (LinearLayout) view.findViewById(R.id.episode_item_view);
                view.setOnClickListener(this);

            }


            @Override
            public void onClick(View v) {
//            Toast.makeText(v.getContext(), "clickingggg", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
