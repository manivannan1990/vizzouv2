package com.dev.vizzou.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.dev.vizzou.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by ceino on 22/4/16.
 */
public class AccountVerifyActivity extends Activity implements View.OnClickListener {

    @InjectView(R.id.close_page)LinearLayout closepage;
    @InjectView(R.id.verifyMobile)RelativeLayout verifyMobile;
    @InjectView(R.id.verifyEmail)RelativeLayout verifyEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accverify);
        ButterKnife.inject(this);
        closepage.setOnClickListener(this);
        verifyMobile.setOnClickListener(this);
        verifyEmail.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.close_page:
                this.finish();
                break;
            case R.id.verifyMobile:
                Intent intent = new Intent(AccountVerifyActivity.this,PhoneVerifyActivity.class);
                startActivity(intent);
                break;
            case R.id.verifyEmail:

                break;
        }
    }
}
