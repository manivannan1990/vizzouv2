package com.dev.vizzou.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.vizzou.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by ceino on 22/4/16.
 */
public class PhoneVerifyActivity extends Activity implements View.OnClickListener {

    @InjectView(R.id.OTPgenFrame)RelativeLayout OTPgenFrame;
    @InjectView(R.id.generateOTP)TextView generateOTP;
    @InjectView(R.id.OTPverifyFrame)RelativeLayout OTPverifyFrame;
    @InjectView(R.id.verifyOTP)TextView verifyOTP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phoneverify);
        ButterKnife.inject(this);
        generateOTP.setOnClickListener(this);
        verifyOTP.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.generateOTP:
                OTPgenFrame.setVisibility(View.GONE);
                OTPverifyFrame.setVisibility(View.VISIBLE);
//                Toast.makeText(PhoneVerifyActivity.this, "Service is in underconstruction", Toast.LENGTH_SHORT).show();
                break;
            case R.id.verifyOTP:
                Toast.makeText(PhoneVerifyActivity.this, "Service is in underconstruction", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
