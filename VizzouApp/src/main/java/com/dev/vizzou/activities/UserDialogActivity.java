package com.dev.vizzou.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.dev.vizzou.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by ceino on 22/4/16.
 */
public class UserDialogActivity extends Activity implements View.OnClickListener {

    @InjectView(R.id.close_page)LinearLayout closepage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userdialog);
        ButterKnife.inject(this);
        closepage.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.close_page:
                this.finish();
                break;
        }
    }
}
