package com.dev.vizzou.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dev.vizzou.R;
import com.dev.vizzou.common.AppConstants;
import com.dev.vizzou.common.BackgroundTask;
import com.dev.vizzou.common.OnTaskCompleted;
import com.dev.vizzou.pojo.Friendsitem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import adapters.CardAdapter;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by ceino on 9/3/16.
 */
public class FriendsListActivity extends Activity implements View.OnClickListener, OnTaskCompleted {

    private static final String TAG = "FriendsListActivity";
    int requestcode = 123;

    Context context = this;

    @InjectView(R.id.recyclerview)RecyclerView recyclerView;
    @InjectView(R.id.close_page)LinearLayout closePage;
    @InjectView(R.id.create_friends_list)ImageView create_friends_list;

    List<Friendsitem> friendsList;
    CardAdapter adapter;
    JSONObject params;
    String userid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friendslist);
        ButterKnife.inject(this);

        closePage.setOnClickListener(this);
        create_friends_list.setOnClickListener(this);

        userid = getIntent().getStringExtra(AppConstants.user_id);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setHasFixedSize(true);

        doRequest();
    }

    private void doRequest(){
        try {
            params = new JSONObject();
            params.put("query", "");
            params.put("uuid", userid);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        BackgroundTask bt = new BackgroundTask(context);
        bt.execute(AppConstants.ACTION_GETALLUSER, params.toString());

    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

        ButterKnife.reset(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.close_page:
                finish();
                break;
            case R.id.create_friends_list:
                String data = "";
                List<Friendsitem> stList = ((CardAdapter) adapter)
                        .getFriendsList();
                for (int i = 0; i < stList.size(); i++) {
                    Friendsitem singleFriend = stList.get(i);
                    if (singleFriend.isUsrSelected() == true) {

                        data = data + "," + singleFriend.getUserId().toString();
                        Intent intent = new Intent();
                        intent.putExtra(AppConstants.FRIENDSLIST, data.substring(1));
                        setResult(requestcode, intent);
                        finish();
                    }

                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(AppConstants.FRIENDSLIST, "");
        setResult(requestcode, intent);
        finish();
    }

    @Override
    public void onTaskCompleted(String values, String flag) {

        Log.e("Response",values);
        try {
            JSONObject jsobj = new JSONObject(values);
                JSONArray resjsobj = jsobj.getJSONArray("result");
                Friendsitem item;
                friendsList = new ArrayList<Friendsitem>();

                for (int i = 0; i < resjsobj.length(); i++) {
                    JSONObject obj = resjsobj.getJSONObject(i);

                    item = new Friendsitem();
                    item.setEmail(obj.getString("email"));
                    item.setFirstName(obj.getString("firstname"));
                    item.setLastName(obj.getString("lastname"));
                    item.setLatitude(obj.getString("lattitude"));
                    item.setLongitude(obj.getString("longtiutde"));
                    item.setUserId(obj.getString("uuid"));
                    item.setProfPicture(obj.getString("profilepicture"));

                    friendsList.add(item);

                }
                adapter = new CardAdapter(friendsList);
                recyclerView.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace(

);
        }
    }
}
