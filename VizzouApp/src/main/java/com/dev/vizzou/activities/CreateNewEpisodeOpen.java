package com.dev.vizzou.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.vizzou.MainActivity;
import com.dev.vizzou.R;
import com.dev.vizzou.common.AppConstants;
import com.dev.vizzou.common.BackgroundTask;
import com.dev.vizzou.common.OnTaskCompleted;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import utils.Utils;

/**
 * Created by ceino on 9/3/16.
 */
public class CreateNewEpisodeOpen extends Activity implements View.OnClickListener, OnTaskCompleted {


    private static final String TAG = "CreateNewEpisodeOpen";
    JSONObject params;
    Context context = this;
    private String filePath = null,userid = "";
    List<String> keywordlist = new ArrayList<String>();
    @InjectView(R.id.close_page)ImageView ClosePage;
    @InjectView(R.id.creat_openepisodebtn)ImageView creat_openepisodebtn;
    @InjectView(R.id.episode_name)EditText episode_name;
    @InjectView(R.id.gethintwords)EditText gethintwords;
    @InjectView(R.id.hintlayout)LinearLayout hintlayout;
    @InjectView(R.id.hlayout) utils.HorizontalFlowLayout horizontalFlowLayout;
    @InjectView(R.id.addkeyword)ImageView addkeyword;
    boolean is_camera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_episode_open);
        ButterKnife.inject(this);

        filePath = getIntent().getStringExtra(AppConstants.intent_filePath);
        userid = getIntent().getStringExtra(AppConstants.user_id);
        is_camera = getIntent().getBooleanExtra(AppConstants.is_Camera, false);
        ClosePage.setOnClickListener(this);
        creat_openepisodebtn.setOnClickListener(this);
        addkeyword.setOnClickListener(this);
    }

    private void addKeyword() {
        if(Utils.isNull(gethintwords)){
            Utils.hideSoftKeyboard(this);
            Toast.makeText(CreateNewEpisodeOpen.this, "Please enter keyword", Toast.LENGTH_SHORT).show();
        }else {
            addTextview();
        }
    }


    private void addTextview() {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        final View keywordview = layoutInflater.inflate(R.layout.keyword_layout, null);

        final TextView tv = (TextView) keywordview.findViewById(R.id.txt_keyword);
        tv.setText(gethintwords.getText().toString());
        horizontalFlowLayout.addView(keywordview);
        keywordlist.add(gethintwords.getText().toString());
        gethintwords.setText("");
        View layout = keywordview.findViewById(R.id.keyword_layout);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0;i<keywordlist.size();i++){
                    if(keywordlist.get(i).equalsIgnoreCase(tv.getText().toString())){
                        keywordlist.remove(keywordlist.get(i));
                    }
                }
                horizontalFlowLayout.removeView(keywordview);
            }
        });
    }

    @Override
    public void onTaskCompleted(String values, String flag) {
        Log.e("values-OpenEpi", values);
        //***************Temporary upto service issue fix************************************
        if(values.equalsIgnoreCase("Error occurred! Http Status Code: 500")){
            AppConstants.setToast(CreateNewEpisodeOpen.this, "Your video/image is uploaded succesfully!");

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(AppConstants.isLogged,"1");
            CreateNewEpisodeOpen.this.finish();
            startActivity(intent);
        }
        //***********************************************************************************
        try {
            JSONObject jsobj = new JSONObject(values);
            if(jsobj.has("success")) {
                if (jsobj.getString("success").equalsIgnoreCase("true")) {
                AppConstants.setToast(CreateNewEpisodeOpen.this, "Your video/image is uploaded succesfully!");

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(AppConstants.isLogged,"1");
                    CreateNewEpisodeOpen.this.finish();
                    startActivity(intent);
                }else {
                    Log.e("MyResponse:","fails");
                    Toast.makeText(CreateNewEpisodeOpen.this, jsobj.getString("result").toString(), Toast.LENGTH_SHORT).show();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.close_page:
                finish();
                break;
            case R.id.addkeyword:
                addKeyword();
                break;
            case R.id.creat_openepisodebtn:

                if(AppConstants.isRequired(context,episode_name)){

                }else if(!(keywordlist.size()>0)){
                    Utils.showAlertDialog(this,null,"Keywords not found",false);
                }else{
                    String tag = "";
                    AppConstants.hideSoftKeyboard(context,episode_name);

                    StringBuilder sb = new StringBuilder();
                    for(int i=0;i<keywordlist.size()-1;i++){
                        sb.append(keywordlist.get(i).toString()+",");
                    }
                    String lastword = keywordlist.get(keywordlist.size()-1).toString();
                    sb.append(lastword);

                    try {
                        params = new JSONObject();
                        params.put("videofile", filePath);

//                        params.put("imagefile",getImagefilepath(filePath));
                        JSONObject para1 = new JSONObject();

                        para1.put("uuid", userid);

                        if(is_camera){
                            tag = AppConstants.UPLOADIMAGE;
                        }else {
                            tag = AppConstants.UPLOAD;
                        }
                        para1.put("keywords", sb);
                        para1.put("episodename", episode_name.getText().toString());
                        para1.put("tag", "OPEN");
                        para1.put("releasedate", "02/05/2016 14:00");

                        params.put("parameters",para1);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    BackgroundTask bt = new BackgroundTask(context,tag);
                    bt.execute(AppConstants.ACTION_CREATEEPISODE, params.toString());
                    break;
                }
                break;
        }
    }

    private File getImagefilepath(String filePath) {

        Bitmap bMap = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Video.Thumbnails.MICRO_KIND);

        Date d = new Date();
        String timestamp = String.valueOf(d.getTime());
        File thumbnail_file = new File(this.getCacheDir(), timestamp+".png");
        try {
            thumbnail_file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Convert bitmap to byte array
        Bitmap bitmap = bMap;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(thumbnail_file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            fos.write(bitmapdata);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return thumbnail_file;
    }
}
