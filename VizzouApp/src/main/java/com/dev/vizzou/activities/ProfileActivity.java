package com.dev.vizzou.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dev.vizzou.R;
import com.dev.vizzou.common.AppConstants;
import com.dev.vizzou.common.BackgroundTask;
import com.dev.vizzou.common.OnTaskCompleted;
import com.dev.vizzou.pojo.User;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import connection.AppConfig;
import utils.Utils;

/**
 * Created by ceino on 23/3/16.
 */
public class ProfileActivity extends Activity implements View.OnClickListener, OnTaskCompleted {

    private static final String TAG = "ProfileActivity";

    Context context = this;

    JSONObject params;

    @InjectView(R.id.findfriends)RelativeLayout findFriends;
    @InjectView(R.id.notification)RelativeLayout notification;
    @InjectView(R.id.settings)RelativeLayout settings;
    @InjectView(R.id.username)TextView username;
    @InjectView(R.id.profimage)ImageView profimage;
    @InjectView(R.id.viewcount)TextView viewcount;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.inject(this);

        findFriends.setOnClickListener(this);
        notification.setOnClickListener(this);
        settings.setOnClickListener(this);


    }

    private void doRequest(){
        sharedpreferences = this.getSharedPreferences(AppConfig.MYPREFERENCES, Context.MODE_PRIVATE);
        String userid = sharedpreferences.getString(AppConfig.ID, "");
        try {
            params = new JSONObject();
            params.put("uuid", userid);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        BackgroundTask bt = new BackgroundTask(context);
        bt.execute(AppConstants.ACTION_GETUSERPROFILE, params.toString());

    }

    @Override
    protected void onResume() {
        super.onResume();
        doRequest();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.findfriends:
                Intent intent1 = new Intent(this, FindFriendsActivity.class);
                startActivity(intent1);
                break;
            case R.id.notification:
                Intent intent2 = new Intent(this, NotificationActivity.class);
                startActivity(intent2);
                break;
            case R.id.settings:
                Intent intent3 = new Intent(this, SettingsActivity.class);
                startActivity(intent3);
                break;
        }
    }

    @Override
    public void onTaskCompleted(String values, String flag) {
        Log.e("Response", values);
        User user;
        try {
            JSONObject jsobj = new JSONObject(values);
            if(jsobj.has("success")) {
                if (jsobj.getString("success").equalsIgnoreCase("true")) {
                    JSONArray jsarr = jsobj.getJSONArray("result");
                    for (int i = 0; i < jsarr.length(); i++) {
                        JSONObject obj = jsarr.getJSONObject(0);
                        username.setText(obj.getString("firstname"));
                        if(Utils.isValid(obj.getString("profilepicture"))) {
                            String temp = obj.getString("profilepicture");
                            temp = temp.replaceAll(" ", "%20");
                            Picasso.with(this).load(AppConstants.IMAGE_URL + temp).placeholder(R.drawable.bg_user)
                                    .error(R.drawable.bg_user)
                                    .fit()
                                    .into(profimage);
                        }else {
                            profimage.setImageResource(R.drawable.bg_user);
                        }
                        viewcount.setText(obj.getString("viewcount")+" Views");
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*@Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(AppConstants.isLogged,"1");
        ProfileActivity.this.finish();
        startActivity(intent);
//        super.onBackPressed();
    }*/
}
