package com.dev.vizzou.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.dev.vizzou.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by ceino on 4/4/16.
 */
public class FindFriendsActivity extends Activity implements View.OnClickListener {

    @InjectView(R.id.close_page)LinearLayout closepage;
    @InjectView(R.id.searchforusrname)RelativeLayout searchforusrname;
    @InjectView(R.id.myfollowers)RelativeLayout myfollowers;
    @InjectView(R.id.addfromphonecontacts)RelativeLayout addfromphonecontacts;
    @InjectView(R.id.invitefriendstovizzou)RelativeLayout invitefriendstovizzou;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_findfriends);
        ButterKnife.inject(this);

        closepage.setOnClickListener(this);
        searchforusrname.setOnClickListener(this);
        myfollowers.setOnClickListener(this);
        addfromphonecontacts.setOnClickListener(this);
        invitefriendstovizzou.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.close_page:
                this.finish();
                break;
            case R.id.searchforusrname:
                Intent intent1 = new Intent(this, SearchUserActivity.class);
                startActivity(intent1);
                break;
            case R.id.myfollowers:

                break;
            case R.id.addfromphonecontacts:

                break;
            case R.id.invitefriendstovizzou:

                break;
        }
    }
}
