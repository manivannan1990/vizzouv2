package com.dev.vizzou.activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.vizzou.R;
import com.dev.vizzou.common.AppConstants;
import com.dev.vizzou.common.BackgroundTask;
import com.dev.vizzou.common.OnTaskCompleted;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import connection.AppConfig;
import utils.RecyclerItemClickListener;
import utils.Utils;
import utils.VizzouBus;

/**
 * Created by ceino on 23/3/16.
 */
public class SearchUserActivity extends Activity implements View.OnClickListener, OnTaskCompleted {

    private static final String TAG = "SearchUserActivity";
    SharedPreferences sharedpreferences;

    @InjectView(R.id.close_page)LinearLayout closepage;
    @InjectView(R.id.searchuser)EditText searchuser;
    @InjectView(R.id.searchimg)ImageView searchImg;
    @InjectView(R.id.recyclerview)RecyclerView recyclerview;
    @InjectView(R.id.userDialogFrame)public LinearLayout userDialogFrame;
    @InjectView(R.id.close_page2)LinearLayout cancelframe;
//    @InjectView(R.id.usrimg)utils.CircleImageView userimage;
    @InjectView(R.id.usrimg)ImageView userimage;
    @InjectView(R.id.username)TextView usrtitle;
    @InjectView(R.id.follow)RelativeLayout followbtn;
    @InjectView(R.id.requested)RelativeLayout requestedbtn;
    @InjectView(R.id.accept)RelativeLayout acceptbtn;
    @InjectView(R.id.unfollow)RelativeLayout unfollowbtn;

    JSONObject params;
    Context context = this;
    String userid= "";

//    List<Friendsitem> searchlist;
    private ArrayList<HashMap<String,String>> searchlist;
    SearchUsrAdapter searchUserAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchuser);
        ButterKnife.inject(this);
        VizzouBus.getInstance().register(this);
        closepage.setOnClickListener(this);
        searchImg.setOnClickListener(this);
        cancelframe.setOnClickListener(this);
        followbtn.setOnClickListener(this);
        acceptbtn.setOnClickListener(this);
        unfollowbtn.setOnClickListener(this);
//        doRequest("k");

        sharedpreferences = this.getSharedPreferences(AppConfig.MYPREFERENCES, Context.MODE_PRIVATE);
        userid = sharedpreferences.getString(AppConfig.ID, "");
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(llm);
        recyclerview.setHasFixedSize(true);

        recyclerview.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                final HashMap<String,String> hmitem =searchlist.get(position);
//                Friendsitem friendsitem = searchlist.get(position);
//                Toast.makeText(SearchUserActivity.this, "position: " + position + "\nusername:" + friendsitem.getFirstName() + " " + friendsitem.getLastName(), Toast.LENGTH_SHORT).show();

                userDialogFrame.setVisibility(View.VISIBLE);
                Animation animation   =    AnimationUtils.loadAnimation(context, R.anim.slide_up);
                animation.setDuration(500);
                userDialogFrame.setAnimation(animation);
                userDialogFrame.animate();
                animation.start();
//                SpannableString styledString = new SpannableString("Follow @\b"+friendsitem.getFirstName()+" "+friendsitem.getLastName());
//                styledString.setSpan(new StyleSpan(Typeface.BOLD),0,8,0);
                if(Utils.isValid(hmitem.get("profilepicture"))) {
                    String temp = hmitem.get("profilepicture");
                    temp = temp.replaceAll(" ", "%20");
                    Picasso.with(context).load(AppConstants.IMAGE_URL + temp).placeholder(R.drawable.placeholderuser)
                            .error(R.drawable.placeholder)
                            .fit()
                            .into(userimage);
                }else {
                    Picasso.with(context).load(R.drawable.placeholderuser).placeholder(R.drawable.placeholderuser)
                            .error(R.drawable.placeholder)
                            .fit()
                            .into(userimage);
                }
//                String s = "Follow @<b>"+hmitem.get("firstname")+" "+hmitem.get("lastname")+"</b>";
//                usrtitle.setText(Html.fromHtml(s));
                usrtitle.setText(hmitem.get("firstname"));
                /*if(hmitem.get("followid") != null)
                    if(hmitem.get("followid").equalsIgnoreCase("1")){
                        unfollowbtn.setVisibility(View.VISIBLE);
                    }else if(hmitem.get("followid").equalsIgnoreCase("0")){
                        followbtn.setVisibility(View.VISIBLE);
                    }else {
                        followbtn.setVisibility(View.VISIBLE);
                    }*/
                AppConstants.FRIEND_ID = hmitem.get("uuid").toString();
                AppConstants.FID = hmitem.get("followid").toString();
                if(hmitem.get("followstatus").equalsIgnoreCase("null")){
                    followbtn.setVisibility(View.VISIBLE);
                    if(acceptbtn.getVisibility()==View.VISIBLE)
                        acceptbtn.setVisibility(View.GONE);
                    if(requestedbtn.getVisibility()==View.VISIBLE)
                        requestedbtn.setVisibility(View.GONE);
                    if(unfollowbtn.getVisibility()==View.VISIBLE)
                        unfollowbtn.setVisibility(View.GONE);
                }else{
                    followbtn.setVisibility(View.GONE);
                    /*if(acceptbtn.getVisibility()==View.VISIBLE)
                        acceptbtn.setVisibility(View.GONE);
                    if(requestedbtn.getVisibility()==View.VISIBLE)
                        requestedbtn.setVisibility(View.GONE);
                    if(unfollowbtn.getVisibility()==View.VISIBLE)
                        unfollowbtn.setVisibility(View.GONE);*/
                }
                if(hmitem.get("followstatus").equalsIgnoreCase("false")){
                    if(hmitem.get("followuser").equalsIgnoreCase(userid)){
                        if(hmitem.get("userrequeststatus").equalsIgnoreCase("Request")){
                            requestedbtn.setVisibility(View.VISIBLE);
                            if(acceptbtn.getVisibility()==View.VISIBLE)
                                acceptbtn.setVisibility(View.GONE);
                            if(followbtn.getVisibility()==View.VISIBLE)
                                followbtn.setVisibility(View.GONE);
                            if(unfollowbtn.getVisibility()==View.VISIBLE)
                                unfollowbtn.setVisibility(View.GONE);
                        }else{
                            requestedbtn.setVisibility(View.GONE);
                            /*if(acceptbtn.getVisibility()==View.VISIBLE)
                                acceptbtn.setVisibility(View.GONE);
                            if(followbtn.getVisibility()==View.VISIBLE)
                                followbtn.setVisibility(View.GONE);
                            if(unfollowbtn.getVisibility()==View.VISIBLE)
                                unfollowbtn.setVisibility(View.GONE);*/
                        }
                    }else if(hmitem.get("followfriend").equalsIgnoreCase(userid)){
                        if(hmitem.get("friendrequeststatus").equalsIgnoreCase("Waiting")){
                            acceptbtn.setVisibility(View.VISIBLE);
                            if(requestedbtn.getVisibility()==View.VISIBLE)
                                requestedbtn.setVisibility(View.GONE);
                            if(followbtn.getVisibility()==View.VISIBLE)
                                followbtn.setVisibility(View.GONE);
                            if(unfollowbtn.getVisibility()==View.VISIBLE)
                                unfollowbtn.setVisibility(View.GONE);
                        }else{
                            acceptbtn.setVisibility(View.GONE);
                            /*if(acceptbtn.getVisibility()==View.VISIBLE)
                                acceptbtn.setVisibility(View.GONE);
                            if(followbtn.getVisibility()==View.VISIBLE)
                                followbtn.setVisibility(View.GONE);
                            if(unfollowbtn.getVisibility()==View.VISIBLE)
                                unfollowbtn.setVisibility(View.GONE);*/
                        }
                    }
                }else{
                    if(hmitem.get("friendrequeststatus").equalsIgnoreCase("Approved")){
                        unfollowbtn.setVisibility(View.VISIBLE);
                        if(requestedbtn.getVisibility()==View.VISIBLE)
                            requestedbtn.setVisibility(View.GONE);
                        if(followbtn.getVisibility()==View.VISIBLE)
                            followbtn.setVisibility(View.GONE);
                        if(acceptbtn.getVisibility()==View.VISIBLE)
                            acceptbtn.setVisibility(View.GONE);
                    }else{
                        unfollowbtn.setVisibility(View.GONE);
                        /*if(acceptbtn.getVisibility()==View.VISIBLE)
                            acceptbtn.setVisibility(View.GONE);
                        if(followbtn.getVisibility()==View.VISIBLE)
                            followbtn.setVisibility(View.GONE);
                        if(requestedbtn.getVisibility()==View.VISIBLE)
                            requestedbtn.setVisibility(View.GONE);*/
                    }
                }



            }
        }));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
        VizzouBus.getInstance().unregister(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(userDialogFrame.getVisibility() == View.VISIBLE){
            userDialogFrame.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        if(userDialogFrame.getVisibility() == View.VISIBLE){
            userDialogFrame.setVisibility(View.INVISIBLE);
            Animation animation   =    AnimationUtils.loadAnimation(context, R.anim.slide_down);
            animation.setDuration(500);
            userDialogFrame.setAnimation(animation);
            userDialogFrame.animate();
            animation.start();
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.close_page:
                this.finish();
                Utils.hideSoftKeyboard(this);
                break;
            case R.id.searchimg:
                String text = searchuser.getText().toString();

                if(text.length()>0){
                    doRequest(searchuser.getText().toString());
                }else {
                    Toast.makeText(SearchUserActivity.this, "Enter keyword ", Toast.LENGTH_SHORT).show();

                }
                Utils.hideSoftKeyboard(this);
                break;
            case R.id.close_page2:
                userDialogFrame.setVisibility(View.INVISIBLE);
                Animation animation   =    AnimationUtils.loadAnimation(context, R.anim.slide_down);
                animation.setDuration(500);
                userDialogFrame.setAnimation(animation);
                userDialogFrame.animate();
                animation.start();
                break;
            case R.id.follow:
                doRequestfollowfriend(AppConstants.FOLLOWFRIEND);
                break;
            case R.id.accept:
                doRequestfollowfriend(AppConstants.ACCEPTFRIEND);
                break;
            case R.id.unfollow:
                doRequestfollowfriend(AppConstants.UNFOLLOWFRIEND);
                break;
        }
    }

    private void doRequest(String s) {
        sharedpreferences = this.getSharedPreferences(AppConfig.MYPREFERENCES, Context.MODE_PRIVATE);
        userid = sharedpreferences.getString(AppConfig.ID, "");
        try {
            params = new JSONObject();
            params.put("query", s);
            params.put("uuid", userid);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        BackgroundTask bt = new BackgroundTask(context);
        bt.execute(AppConstants.ACTION_SEARCHFRIENDS, params.toString());
    }

    private void doRequestfollowfriend(String flag) {
        sharedpreferences = this.getSharedPreferences(AppConfig.MYPREFERENCES, Context.MODE_PRIVATE);
        userid = sharedpreferences.getString(AppConfig.ID, "");
        try {
            params = new JSONObject();
            if(flag.equalsIgnoreCase(AppConstants.FOLLOWFRIEND))
                params.put("userid", userid);
                params.put("friendid", AppConstants.FRIEND_ID);
            if(flag.equalsIgnoreCase(AppConstants.ACCEPTFRIEND))
                params.put("status", true);
                params.put("fId", AppConstants.FID);
            if(flag.equalsIgnoreCase(AppConstants.UNFOLLOWFRIEND))
                params.put("fId", AppConstants.FID);


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        BackgroundTask bt = new BackgroundTask(context,flag);
        if(flag.equalsIgnoreCase(AppConstants.FOLLOWFRIEND))
            bt.execute(AppConstants.ACTION_FOLLOWFRIEND, params.toString());
        if(flag.equalsIgnoreCase(AppConstants.ACCEPTFRIEND))
            bt.execute(AppConstants.ACTION_ACCEPTFRIEND, params.toString());
        if(flag.equalsIgnoreCase(AppConstants.UNFOLLOWFRIEND))
            bt.execute(AppConstants.ACTION_UNFLLOWFRIEND, params.toString());

    }

    @Override
    public void onTaskCompleted(String values, String flag) {

        Log.e("Response", values);

        try {
            if(flag.equalsIgnoreCase(AppConstants.FOLLOWFRIEND)){
                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")) {
                    if (jsobj.getString("success").equalsIgnoreCase("true")) {
                        Toast.makeText(SearchUserActivity.this, "Friend Requested", Toast.LENGTH_SHORT).show();
                        doRequest(searchuser.getText().toString());
                        userDialogFrame.setVisibility(View.GONE);
                    }
                }
            }else if(flag.equalsIgnoreCase(AppConstants.ACCEPTFRIEND)){
                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")) {
                    if (jsobj.getString("success").equalsIgnoreCase("true")) {
                        Toast.makeText(SearchUserActivity.this, "Friend Request Accepted..Both are following now", Toast.LENGTH_SHORT).show();
                        doRequest(searchuser.getText().toString());
                        userDialogFrame.setVisibility(View.GONE);
                    }
                }
            }else if(flag.equalsIgnoreCase(AppConstants.UNFOLLOWFRIEND)){
                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")) {
                    if (jsobj.getString("success").equalsIgnoreCase("true")) {
                        Toast.makeText(SearchUserActivity.this, "Unfollowing now", Toast.LENGTH_SHORT).show();
                        doRequest(searchuser.getText().toString());
                        userDialogFrame.setVisibility(View.GONE);
                    }
                }
            }else {
                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")) {
                    if (jsobj.getString("success").equalsIgnoreCase("true")) {
                        JSONArray jsarr = jsobj.getJSONArray("result");
//                    Friendsitem friendsitem;
                        searchlist = new ArrayList<HashMap<String,String>>();

                        for(int i=0;i<jsarr.length();i++){
                            JSONObject obj = jsarr.getJSONObject(i);
//                        friendsitem = new Friendsitem();
                            HashMap<String,String> hm = new HashMap<String,String>();

                        /*friendsitem.setUserId(obj.getString("uuid"));
                        friendsitem.setEmail(obj.getString("email"));
                        friendsitem.setFirstName(obj.getString("firstname"));
                        friendsitem.setLatitude(obj.getString("lattitude"));
                        friendsitem.setLongitude(obj.getString("longtiutde"));
                        friendsitem.setLastName(obj.getString("lastname"));
                        friendsitem.setProfPicture(obj.getString("profilepicture"));*/
                            hm.put("uuid",obj.getString("uuid"));
                            hm.put("email",obj.getString("email"));
                            hm.put("firstname",obj.getString("firstname"));
                            hm.put("lattitude",obj.getString("lattitude"));
                            hm.put("longtiutde",obj.getString("longtiutde"));
                            hm.put("lastname",obj.getString("lastname"));
                            hm.put("profilepicture",obj.getString("profilepicture"));
                            hm.put("followid",obj.getString("followid"));
                            hm.put("followfriend",obj.getString("followfriend"));
                            hm.put("followuser",obj.getString("followuser"));
                            hm.put("friendrequeststatus",obj.getString("friendrequeststatus"));
                            hm.put("followstatus",obj.getString("followstatus"));
                            hm.put("userrequeststatus",obj.getString("userrequeststatus"));




                            searchlist.add(hm);

                        }
                        searchUserAdapter = new SearchUsrAdapter(context,searchlist);
                        recyclerview.setAdapter(searchUserAdapter);

                    }else {
                        Toast.makeText(this,jsobj.getString("result").toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            }




        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
class SearchUsrAdapter extends RecyclerView.Adapter<SearchUsrAdapter.ViewHolder> {

    ArrayList<HashMap<String,String>> mItems;
    Context mcontext;

    public SearchUsrAdapter(Context context, ArrayList<HashMap<String,String>> items) {
        super();
        this.mItems = items;
        this.mcontext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.search_user_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {

        final HashMap<String,String> hm = mItems.get(i);
        viewHolder.usrName.setText(hm.get("firstname")+" "+hm.get("lastname"));
        if(Utils.isValid(hm.get("profilepicture"))) {
            String temp = hm.get("profilepicture");
            temp = temp.replaceAll(" ", "%20");
            Picasso.with(mcontext).load(AppConstants.IMAGE_URL + temp).placeholder(R.drawable.placeholderuser)
                    .error(R.drawable.placeholder)
                    .into(viewHolder.usrImage);
        }else {
            Picasso.with(mcontext).load(R.drawable.placeholderuser).placeholder(R.drawable.placeholderuser)
                    .error(R.drawable.placeholder)
                    .into(viewHolder.usrImage);
        }

    }

    @Override
    public int getItemCount() {

        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        utils.CircleImageView usrImage;
        TextView usrName;
        RelativeLayout usrItem;

        public ViewHolder(View itemView) {
            super(itemView);
            usrImage = (utils.CircleImageView) itemView.findViewById(R.id.usrimage);
            usrName = (TextView) itemView.findViewById(R.id.usrname);
            usrItem = (RelativeLayout) itemView.findViewById(R.id.useritem);
        }
    }
}
