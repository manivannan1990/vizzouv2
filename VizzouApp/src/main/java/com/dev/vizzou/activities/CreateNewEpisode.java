package com.dev.vizzou.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dev.vizzou.MainActivity;
import com.dev.vizzou.R;
import com.dev.vizzou.common.AppConstants;
import com.dev.vizzou.common.BackgroundTask;
import com.dev.vizzou.common.OnTaskCompleted;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by ceino on 9/3/16.
 */
public class CreateNewEpisode extends Activity implements View.OnClickListener, OnTaskCompleted {


    private static final String TAG = "CreateNewEpisode";
    JSONObject params;
    Context context = this;
    private String filePath = null,userid = "";
    String DateTimeStr = "";
    @InjectView(R.id.close_page)ImageView ClosePage;
    @InjectView(R.id.creat_episodebtn)ImageView create_episode;
    @InjectView(R.id.datePicker1)DatePicker dp;
    @InjectView(R.id.timePicker1)TimePicker tp;
    @InjectView(R.id.episode_name)EditText episode_name;
    boolean is_camera;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_episode);
        ButterKnife.inject(this);

        filePath = getIntent().getStringExtra(AppConstants.intent_filePath);
        userid = getIntent().getStringExtra(AppConstants.user_id);
        is_camera = getIntent().getBooleanExtra(AppConstants.is_Camera, false);

        ClosePage.setOnClickListener(this);
        create_episode.setOnClickListener(this);

        dp.setMinDate(System.currentTimeMillis() - 1000);
    }

    @Override
    public void onTaskCompleted(String values, String flag) {
        Log.e("values-MyEpi",values);
        //***************Temporary upto service issue fix************************************
        if(values.equalsIgnoreCase("Error occurred! Http Status Code: 500")){
            AppConstants.setToast(CreateNewEpisode.this, "Your video/image is uploaded succesfully!");

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(AppConstants.isLogged,"1");
            CreateNewEpisode.this.finish();
            startActivity(intent);
        }
        //***********************************************************************************
        try {
            JSONObject jsobj = new JSONObject(values);
            if(jsobj.has("success")) {
                if (jsobj.getString("success").equalsIgnoreCase("true")) {
                AppConstants.setToast(CreateNewEpisode.this, "Your video/image is uploaded succesfully!");

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(AppConstants.isLogged,"1");
                    CreateNewEpisode.this.finish();
                    startActivity(intent);
                }else {
                    Log.e("MyResponse:","fails");
                    Toast.makeText(CreateNewEpisode.this, jsobj.getString("result").toString(), Toast.LENGTH_SHORT).show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.close_page:
                finish();
                break;
            case R.id.creat_episodebtn:
                if(AppConstants.isRequired(context,episode_name)){

                }else{
                    String tag = "";
                    AppConstants.hideSoftKeyboard(context,episode_name);
                    DateTimeStr = (dp.getMonth() + 1) + "/" + dp.getDayOfMonth() + "/" + dp.getYear() + " "+ tp.getCurrentHour() + ":" + tp.getCurrentMinute();

                    try {
                        params = new JSONObject();
                        params.put("videofile", filePath);

                        JSONObject para1 = new JSONObject();

                        para1.put("uuid", userid);
                        if(is_camera){
                            tag = AppConstants.UPLOADIMAGE;
                        }else {
                            tag = AppConstants.UPLOAD;
                        }
                        para1.put("keywords", "");
                        para1.put("releasedate", DateTimeStr);
                        para1.put("episodename", episode_name.getText().toString());
                        para1.put("tag", "PRIVATE");

                        params.put("parameters",para1);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    BackgroundTask bt = new BackgroundTask(context,tag);
                    bt.execute(AppConstants.ACTION_CREATEEPISODE, params.toString());
                    break;
                }

        }
    }
}
