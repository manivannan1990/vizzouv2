package com.dev.vizzou.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dev.vizzou.R;
import com.squareup.otto.Subscribe;

import butterknife.ButterKnife;
import butterknife.InjectView;
import connection.AppConfig;
import events.VSignOutEvent;
import utils.VizzouBus;

/**
 * Created by ceino on 4/4/16.
 */
public class SettingsActivity extends Activity implements View.OnClickListener {

    @InjectView(R.id.close_page)LinearLayout closepage;
    @InjectView(R.id.logout)RelativeLayout logout;
    @InjectView(R.id.etxtProfile)RelativeLayout editProfile;
    @InjectView(R.id.getintouch)RelativeLayout getintouch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.inject(this);
        VizzouBus.getInstance().register(this);
        logout.setOnClickListener(this);
        closepage.setOnClickListener(this);
        editProfile.setOnClickListener(this);
        getintouch.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
        VizzouBus.getInstance().unregister(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.logout:
                showAlertDialogLogOut();
                break;
            case R.id.close_page:
                this.finish();
                break;
            case R.id.etxtProfile:
                Intent intent1 = new Intent(SettingsActivity.this,EditProfileActivity.class);
                startActivity(intent1);
                break;
            case R.id.getintouch:
                //Please include whatever email your wish
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:support@gmail.com"));
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Vizzpu app");
                intent.putExtra(Intent.EXTRA_TEXT, "Comments");
                startActivity(intent);
                break;
        }
    }
    private void showAlertDialogLogOut(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);
//        alertDialog.setTitle("LOGOUT");
        alertDialog.setMessage("Are you sure you want logout");
        alertDialog.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(getApplicationContext(), "LogOut", Toast.LENGTH_SHORT).show();
//                VizzouService.startActionSignOUT(SettingsActivity.this);
                VizzouBus.getInstance().post(new VSignOutEvent(true));
                SharedPreferences sharedpreferences = getSharedPreferences(AppConfig.MYPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(AppConfig.EMAIL, "");
                editor.putString(AppConfig.ID, "");
                editor.putString(AppConfig.NAME, "");
                editor.clear();
                editor.commit();
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Subscribe
    public void onVSignOutEvent(VSignOutEvent event) {
        if(event.isSuccess){
            Toast.makeText(SettingsActivity.this, "Logout", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            SettingsActivity.this.finish();
            startActivity(intent);
        }

    }
}
