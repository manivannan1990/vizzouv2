package com.dev.vizzou.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.vizzou.R;
import com.dev.vizzou.common.AppConstants;
import com.dev.vizzou.common.CustomVideoView;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import utils.camerautils.AndroidMultiPartEntity;

public class UploadActivity extends Activity {
    // LogCat tag
    private static final String TAG = "tag";

    private ProgressBar progressBar;
    private String filePath = null,userid = "";
    private String previewpath = null;
    private TextView txtPercentage;
    private CustomVideoView vidPreview;
    private ImageView imagePreview;
    private ImageView btnUpload,btnClose;
    long totalSize = 0;
    boolean is_camera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        txtPercentage = (TextView) findViewById(R.id.txtPercentage);
        btnUpload = (ImageView) findViewById(R.id.btnUpload);
        btnClose = (ImageView) findViewById(R.id.btnClose);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        vidPreview = (CustomVideoView) findViewById(R.id.videoPreview);
        imagePreview = (ImageView) findViewById(R.id.imagePreview);

        // Receiving the data from previous activity
        Intent i = getIntent();

        // image or video path that is captured in previous activity
        filePath = i.getStringExtra(AppConstants.intent_filePath);
        previewpath = i.getStringExtra(AppConstants.intent_previewPath);
        userid = i.getStringExtra(AppConstants.user_id);
        is_camera = i.getBooleanExtra(AppConstants.is_Camera,true);


        if (filePath != null) {
            // Displaying the image or video on the screen
            previewMedia();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
        }

        btnUpload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // uploading the file to server
//                new UploadFileToServer().execute();
                Intent i = new Intent(getApplicationContext(), AddEpisodeActivity.class);
                i.putExtra(AppConstants.intent_filePath, getIntent().getStringExtra(AppConstants.intent_filePath));
                i.putExtra(AppConstants.user_id, getIntent().getStringExtra(AppConstants.user_id));
                if(is_camera){
                    i.putExtra(AppConstants.is_Camera,true);
                }else {
                    i.putExtra(AppConstants.is_Camera,false);
                }
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });


        btnClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // uploading the file to server
                UploadActivity.this.finish();
            }
        });

        vidPreview.setPlayPauseListener(new CustomVideoView.PlayPauseListener() {

            @Override
            public void onPlay() {
                System.out.println("Play!");
            }

            @Override
            public void onPause() {
                System.out.println("Pause!");
            }
        });

    }

    /**
     * Displaying captured image/video on the screen
     * */
    private void previewMedia() {
        if(is_camera){
            vidPreview.setVisibility(View.GONE);
            imagePreview.setVisibility(View.VISIBLE);
            Picasso.with(UploadActivity.this).load(previewpath)
//                    .fit().centerInside()
                    .rotate(90)
                    .error(R.drawable.placeholder)
                    .into(imagePreview);
        }else{
            imagePreview.setVisibility(View.GONE);
            vidPreview.setVisibility(View.VISIBLE);
            vidPreview.setMediaController(new MediaController(this));
            vidPreview.setVideoPath(filePath);
            // start playing
            vidPreview.start();
        }

    }

    /**
     * Uploading the file to server
     * */
    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            progressBar.setProgress(0);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible
            progressBar.setVisibility(View.VISIBLE);

            // updating progress bar value
            progressBar.setProgress(progress[0]);

            // updating percentage value
            txtPercentage.setText(String.valueOf(progress[0]) + "%");
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://52.32.183.109:8080/vizzou/UPLOADVIDEO/VIDEO");

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                File sourceFile = new File(filePath);

                Bitmap bMap = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Video.Thumbnails.MICRO_KIND);

                Date d = new Date();
                String timestamp = String.valueOf(d.getTime());
                File thumbnail_file = new File(UploadActivity.this.getCacheDir(), timestamp+".png");
                thumbnail_file.createNewFile();

                //Convert bitmap to byte array
                Bitmap bitmap = bMap;
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();

                //write the bytes in file
                FileOutputStream fos = new FileOutputStream(thumbnail_file);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();

                // Adding file data to http body
                entity.addPart("file", new FileBody(sourceFile));
                entity.addPart("imagefile", new FileBody(thumbnail_file));

                // Extra parameters if you want to pass to server
                entity.addPart("userid", new StringBody(userid));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e(TAG, "Response from server: " + result);
            try {
                JSONObject jsobj = new JSONObject(result);
                if(jsobj.getString("success").equalsIgnoreCase("true")){
                    Toast.makeText(UploadActivity.this, "Your video is uploaded succesfully!", Toast.LENGTH_SHORT).show();
                    UploadActivity.this.finish();
                    Intent intent = new Intent(getApplicationContext(), AddEpisodeActivity.class);
                    startActivity(intent);
                }else{

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            super.onPostExecute(result);
        }

    }

}