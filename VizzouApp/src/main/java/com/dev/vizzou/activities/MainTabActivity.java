package com.dev.vizzou.activities;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import com.dev.vizzou.R;

/**
 * Created by ceino on 21/3/16.
 */
@SuppressWarnings("deprecation")
public class MainTabActivity extends TabActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hometab);

        Resources ressources = getResources();
        TabHost tabHost = getTabHost();

        // Android tab
        Intent intentHome = new Intent().setClass(this, HomeActivity.class);
        TabSpec tabSpecHome = tabHost
                .newTabSpec("Home")
                .setIndicator(prepareTabView("", R.drawable.tab_home_group))
                .setContent(intentHome);

        // Apple tab
        Intent intentSearch = new Intent().setClass(this, SearchActivity.class);
        TabSpec tabSpecSearch = tabHost
                .newTabSpec("Search")
//                .setIndicator("", ressources.getDrawable(R.drawable.tab_search_group))
                .setIndicator(prepareTabView("", R.drawable.tab_search_group))
                .setContent(intentSearch);

        // Windows tab
        /*Intent intentChat = new Intent().setClass(this, ChatActivity.class);
        TabSpec tabSpecChat = tabHost
                .newTabSpec("Chat")
//                .setIndicator("", ressources.getDrawable(R.drawable.tab_message_group))
                .setIndicator(prepareTabView("", R.drawable.tab_message_group))
                .setContent(intentChat);*/

        // Blackberry tab
        Intent intentProfile = new Intent().setClass(this, ProfileActivity.class);
        TabSpec tabSpecProfile = tabHost
                .newTabSpec("User")
//                .setIndicator("", ressources.getDrawable(R.drawable.tab_user_group))
                .setIndicator(prepareTabView("", R.drawable.tab_user_group))
                .setContent(intentProfile);

        // add all tabs
        tabHost.addTab(tabSpecHome);
        tabHost.addTab(tabSpecSearch);
//        tabHost.addTab(tabSpecChat);
        tabHost.addTab(tabSpecProfile);

        //set Windows tab as default (zero based)
        tabHost.setCurrentTab(0);
    }
    private View prepareTabView(String text, int resId) {
        View view = LayoutInflater.from(this).inflate(R.layout.tabs, null);
        ImageView iv = (ImageView) view.findViewById(R.id.TabImageView);
        iv.setImageResource(resId);
        return view;
    }
}
