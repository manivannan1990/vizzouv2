package com.dev.vizzou.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dev.vizzou.R;
import com.dev.vizzou.common.AppConstants;
import com.dev.vizzou.common.BackgroundTask;
import com.dev.vizzou.common.OnTaskCompleted;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import connection.AppConfig;
import utils.Constants;
import utils.Utils;

/**
 * Created by ceino on 22/4/16.
 */
@SuppressWarnings("deprecation")
public class EditProfileActivity extends Activity implements View.OnClickListener,OnTaskCompleted {

    @InjectView(R.id.close_page)LinearLayout closepage;
    @InjectView(R.id.updatebtn)ImageView updatebtn;
    @InjectView(R.id.accVerification)RelativeLayout accVerifybtn;
    @InjectView(R.id.profimage)ImageView usrimage;
    @InjectView(R.id.fullname)EditText usrfullname;
    @InjectView(R.id.username)EditText usrname;
    @InjectView(R.id.usrpwd)EditText usrpwd;
    @InjectView(R.id.usremail)EditText usremail;
    @InjectView(R.id.usrphone)EditText usrphone;

    Context context = this;

    JSONObject params;

    public String image_path,serverImg = null;
    public Bitmap bitmap;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprof);
        ButterKnife.inject(this);
        doRequest(AppConstants.VIEW_USER_PROFILE);

        closepage.setOnClickListener(this);
        updatebtn.setOnClickListener(this);
        accVerifybtn.setOnClickListener(this);
        usrimage.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.profimage:
                selectImage();
                break;
            case R.id.close_page:
                this.finish();
                break;
            case R.id.updatebtn:

                if(Utils.isNull(usrfullname))
                    usrfullname.setError("Enter Name");
                else if(Utils.isNull(usrname))
                    usrname.setError("Enter Username");
                else if(Utils.isNull(usrpwd))
                    usrpwd.setError("Enter Password");
                else if(Utils.isNull(usremail))
                    usremail.setError("Enter Email");
                else if(Utils.isNull(usrphone))
                    usrphone.setError("Enter Phone#");
                else {
                    if(image_path == null){
                        image_path = serverImg;
                    }
                    doRequest(AppConstants.UPDATE_USER);
                }

                Utils.hideSoftKeyboard(this);
                break;
            case R.id.accVerification:
                Intent intent = new Intent(EditProfileActivity.this,AccountVerifyActivity.class);
                startActivity(intent);
                break;
        }
    }
    private void doRequest(String tag){
        sharedpreferences = this.getSharedPreferences(AppConfig.MYPREFERENCES, Context.MODE_PRIVATE);
        String userid = sharedpreferences.getString(AppConfig.ID, "");
        try {
            params = new JSONObject();
            if(tag.equalsIgnoreCase(AppConstants.VIEW_USER_PROFILE)){
                params.put("uuid", userid);
            }
            if(tag.equalsIgnoreCase(AppConstants.UPDATE_USER)){

                params.put("profilepicture", image_path);
                JSONObject para1 = new JSONObject();
                para1.put("uuid", userid);
                para1.put("firstname", usrname.getText().toString());
                para1.put("lastname", usrfullname.getText().toString());
                para1.put("email", usremail.getText().toString());
                para1.put("phoneno", usrphone.getText().toString());
                para1.put("password", usrpwd.getText().toString());
                params.put("parameters",para1);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        BackgroundTask bt = new BackgroundTask(context,tag);
        if(tag.equalsIgnoreCase(AppConstants.VIEW_USER_PROFILE))
            bt.execute(AppConstants.ACTION_GETUSERPROFILE, params.toString());
        if (tag.equalsIgnoreCase(AppConstants.UPDATE_USER))
            bt.execute(AppConstants.ACTION_UPDATEUSER, params.toString());

    }

    @Override
    public void onTaskCompleted(String values, String flag) {

        try {
            if(flag.equalsIgnoreCase(AppConstants.VIEW_USER_PROFILE)){
                Log.e("Response", values);
                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")) {
                    if (jsobj.getString("success").equalsIgnoreCase("true")) {
                        JSONArray jsarr = jsobj.getJSONArray("result");
                        for (int i = 0; i < jsarr.length(); i++) {
                            JSONObject obj = jsarr.getJSONObject(0);
                            if(Utils.isValid(obj.getString("profilepicture"))){
                                String temp = obj.getString("profilepicture");
                                temp = temp.replaceAll(" ", "%20");


                                Picasso.with(this).load(AppConstants.IMAGE_URL + temp).placeholder(R.drawable.bg_user)
                                        .error(R.drawable.bg_user)
                                        .resize(200,200)
                                        .into(usrimage);
                                Bitmap bitmap = ((BitmapDrawable)usrimage.getDrawable()).getBitmap();
                                Date d = new Date();
                                String timestamp = String.valueOf(d.getTime());
                                File f = new File(context.getCacheDir(), timestamp+"temp.png");
                                try {
                                    f.createNewFile();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                                byte[] bitmapdata = bos.toByteArray();
                                FileOutputStream fos = null;
                                try {
                                    fos = new FileOutputStream(f);
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fos.write(bitmapdata);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fos.flush();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fos.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                serverImg = f.toString();
                            }else {
                                Drawable drawable = this.getResources().getDrawable(R.drawable.bg_user);
                                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                                Date d = new Date();
                                String timestamp = String.valueOf(d.getTime());
                                File f = new File(context.getCacheDir(), timestamp+"temp.png");
                                try {
                                    f.createNewFile();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                                byte[] bitmapdata = bos.toByteArray();
                                FileOutputStream fos = null;
                                try {
                                    fos = new FileOutputStream(f);
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fos.write(bitmapdata);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fos.flush();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fos.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
//                                Uri path = Uri.parse("android.resource://com.dev.vizzou/" + R.drawable.bg_user+".png");
                                serverImg = f.toString();
                            }
                            if(Utils.isValid(obj.getString("lastname")))
                                usrfullname.setText(obj.getString("lastname"));
                            if(Utils.isValid(obj.getString("firstname")))
                                usrname.setText(obj.getString("firstname"));
                            if(Utils.isValid(obj.getString("password")))
                                usrpwd.setText(obj.getString("password"));
                            if(Utils.isValid(obj.getString("email")))
                                usremail.setText(obj.getString("email"));
                            if(Utils.isValid(obj.getString("phoneno")))
                                usrphone.setText(obj.getString("phoneno"));

//                            Toast.makeText(EditProfileActivity.this, "Profile setted from server", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }else if(flag.equalsIgnoreCase(AppConstants.UPDATE_USER)){
                Log.e("Response:updateuser", values);
                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")) {
                    if (jsobj.getString("success").equalsIgnoreCase("true")) {
                        JSONArray jsarr = jsobj.getJSONArray("result");
                        for (int i = 0; i < jsarr.length(); i++) {
                            JSONObject obj = jsarr.getJSONObject(0);
                            usrfullname.setText(obj.getString("lastname"));
                            usrname.setText(obj.getString("firstname"));
                            usrpwd.setText(obj.getString("password"));
                            usremail.setText(obj.getString("email"));
                            usrphone.setText(obj.getString("phoneno"));
                            if(Utils.isValid(obj.getString("profilepicture"))){
                                String temp = obj.getString("profilepicture");
                                temp = temp.replaceAll(" ", "%20");
                                Picasso.with(this).load(AppConstants.IMAGE_URL + temp).placeholder(R.drawable.bg_user)
                                        .error(R.drawable.nointernet)
                                        .resize(200,200)
                                        .into(usrimage);
                            }
                            /*vizzouApplication.editVizzouAccessToken(obj.getString("profilepicture"), obj.getString("lastname"),
                                    obj.getString("firstname"), obj.getString("password"), obj.getString("email"), obj.getString("phoneno"));*/
                            Toast.makeText(EditProfileActivity.this, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void selectImage() {

        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo"))
                {
                    Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, Constants.CAMERA);
                }
                else if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, Constants.GALLERY);
                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.CAMERA) {
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");


                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                    image_path = String.valueOf(destination);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                usrimage.setImageBitmap(bitmap);
            } else if (requestCode == Constants.GALLERY) {
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = this.getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                image_path = picturePath;
                c.close();
                bitmap = (BitmapFactory.decodeFile(picturePath));
                Log.w("path of image from gallery......******************.........", picturePath + "");
                usrimage.setImageBitmap(bitmap);
            }
        }

    }
}
