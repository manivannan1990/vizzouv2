package com.dev.vizzou.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dev.vizzou.R;
import com.dev.vizzou.common.AppConstants;
import com.dev.vizzou.common.BackgroundTask;
import com.dev.vizzou.common.OnTaskCompleted;
import com.dev.vizzou.videostream.VideoPlayerActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import connection.AppConfig;
import utils.VizzouBus;

/**
 * Created by ceino on 23/3/16.
 */
public class SearchActivity extends Activity implements View.OnClickListener, OnTaskCompleted {

    private static final String TAG = "SearchActivity";

    SharedPreferences sharedpreferences;

    @InjectView(R.id.recycler_view)RecyclerView mRecyclerView;

    private ArrayList<HashMap<String,String>> dataList;
    JSONObject params;
    Context context = this;
    String userid= "";
    SearchAdapter adapter;

    private final int MAX_CARDS = 3;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.inject(this);
        VizzouBus.getInstance().register(this);

        mRecyclerView.setHasFixedSize(true);


        GridLayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(mLayoutManager);

        doRequest();

    }

    public void doRequest() {
        sharedpreferences = this.getSharedPreferences(AppConfig.MYPREFERENCES, Context.MODE_PRIVATE);
        userid = sharedpreferences.getString(AppConfig.ID, "");
        try {
            params = new JSONObject();
            params.put("Offset", "0");
            params.put("Limit", "0");
            params.put("query", "time");
            params.put("userid", userid);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("params", params.toString());

        BackgroundTask bt = new BackgroundTask(context);
        bt.execute(AppConstants.ACTION_SEARCHEPISODES, params.toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        doRequest();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
        VizzouBus.getInstance().unregister(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        doRequest();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
        }
    }


    @Override
    public void onTaskCompleted(String values, String flag) {
        Log.e("Response",values);
        try {
            JSONObject jsobj = new JSONObject(values);
            JSONArray resjsobj = jsobj.getJSONArray("result");
            dataList = new ArrayList<HashMap<String,String>>();

            for (int i = 0; i < resjsobj.length(); i++) {
                JSONObject obj = resjsobj.getJSONObject(i);

                HashMap<String,String> hm = new HashMap<String,String>();

                if(!userid.equalsIgnoreCase(obj.getString("created_by"))){
                    hm.put("episodeid",obj.getString("episodeid"));
                    hm.put("episodename",obj.getString("episodename"));
                    hm.put("firstname",obj.getString("firstname"));
                    hm.put("videoname",obj.getString("videoname"));
                    hm.put("created_on",obj.getString("created_on"));
                    hm.put("releasedate",obj.getString("releasedate"));
                    hm.put("userid",obj.getString("userid"));
                    hm.put("imagename",obj.getString("imagename"));
                    hm.put("type",obj.getString("type"));
                    hm.put("feid",obj.getString("feid"));

                    dataList.add(hm);
                }

            }
            adapter = new SearchAdapter(context,dataList);
            mRecyclerView.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

class Item {
    private int position;

    protected Item(int position) {
        this.position = position;
    }

    public String getPositionText() {
        return Integer.toString(position);
    }
}


class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    ArrayList<HashMap<String,String>> mItems;
    Context mcontext;

    public SearchAdapter(Context context, ArrayList<HashMap<String,String>> items) {
        super();
        this.mItems = items;
        this.mcontext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.simplest_card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {

        final HashMap<String,String> hm = mItems.get(i);
        viewHolder.name.setText(hm.get("episodename"));
        Picasso.with(mcontext).load(AppConstants.IMAGE_URL + hm.get("imagename"))
                .error(R.drawable.placeholder)
                .placeholder(R.drawable.placeholder)
                .into(viewHolder.imgThumbnail);
        viewHolder.search_episode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = AppConstants.VIDEO_URL + hm.get("videoname");
                Intent intent = new Intent(mcontext, VideoPlayerActivity.class);
                intent.putExtra("URL", url);
                AppConstants.EPISODE_ID = hm.get("episodeid");
                AppConstants.FRIEND_ID = hm.get("userid");
                AppConstants.FE_ID = hm.get("feid");
                if(hm.get("type") != null){
                    if(hm.get("type").equalsIgnoreCase("JOINOPEN")){
                        AppConstants.TYPE = "UNJOINOPEN";
                    }else {
                        AppConstants.TYPE = "JOINOPEN";
                    }
                }else {
                    AppConstants.TYPE = "JOINOPEN";
                }
                ((Activity) mcontext).startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {

        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imgThumbnail;
        public TextView name;
        public LinearLayout search_episode;

        public ViewHolder(View itemView) {
            super(itemView);
            imgThumbnail = (ImageView) itemView.findViewById(R.id.image);
            name = (TextView) itemView.findViewById(R.id.name);
            search_episode = (LinearLayout) itemView.findViewById(R.id.search_episode);
        }
    }
}
