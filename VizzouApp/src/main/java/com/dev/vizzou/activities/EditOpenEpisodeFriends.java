package com.dev.vizzou.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dev.vizzou.MainActivity;
import com.dev.vizzou.R;
import com.dev.vizzou.common.AppConstants;
import com.dev.vizzou.common.BackgroundTask;
import com.dev.vizzou.common.OnTaskCompleted;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import data.episode_videos;

/**
 * Created by ceino on 9/3/16.
 */
public class EditOpenEpisodeFriends extends Activity implements View.OnClickListener, OnTaskCompleted {


    private static final String TAG = "EditOpenEpisodeFriends";
    int requestcode = 123;
    JSONObject params;
    Context context = this;
    private String filePath = null,userid = "";
    String DateTimeStr = "";
    String friendsList = "";
    String keywords = "";
    boolean is_camera;


    @InjectView(R.id.close_page)ImageView ClosePage;
    @InjectView(R.id.addFriendsList)RelativeLayout addFriendslist;
    @InjectView(R.id.create_episode)ImageView create_episode;
    @InjectView(R.id.datePicker2)DatePicker dp;
    @InjectView(R.id.timePicker2)TimePicker tp;
    @InjectView(R.id.episode_name)EditText episode_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_episode_friends);

//        filePath = getIntent().getStringExtra(AppConstants.intent_filePath);
        final episode_videos model = (episode_videos) getIntent().getSerializableExtra("MyClass");
        userid = getIntent().getStringExtra(AppConstants.user_id);
        is_camera = getIntent().getBooleanExtra(AppConstants.is_Camera,false);

        ButterKnife.inject(this);
        ClosePage.setOnClickListener(this);
        create_episode.setOnClickListener(this);
        addFriendslist.setOnClickListener(this);

        dp.setMinDate(System.currentTimeMillis() - 1000);
        episode_name.setText(model.getEpisodename());
        friendsList = model.getFriendid();
        keywords = model.getKeywords();

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");

        Log.e("Releasedate",model.getReleasedate());
        try {

           Date start = dateFormat.parse(model.getReleasedate());
            dp.updateDate(start.getYear(),start.getMonth(),start.getDay());
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }

    @Override
    public void onTaskCompleted(String values, String flag) {
        try {
            JSONObject jsobj = new JSONObject(values);
            if(jsobj.has("success")) {
                if (jsobj.getString("success").equalsIgnoreCase("true")) {
                AppConstants.setToast(EditOpenEpisodeFriends.this, "Your video/image is uploaded succesfully!");

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(AppConstants.isLogged,"1");
                    EditOpenEpisodeFriends.this.finish();
                    startActivity(intent);
                }else {
                    Log.e("MyResponse:","fails");
                    Toast.makeText(EditOpenEpisodeFriends.this, jsobj.getString("result").toString(), Toast.LENGTH_SHORT).show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == requestcode) {
            try{
                friendsList = new String();
                friendsList = data.getStringExtra(AppConstants.FRIENDSLIST);
            }catch (Exception e){}
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.close_page:
                finish();
                break;
            case R.id.addFriendsList:
                Intent intent = new Intent(this, FriendsListActivity.class);
                intent.putExtra(AppConstants.user_id, userid);
                startActivityForResult(intent, requestcode);
                break;
            case R.id.create_episode:
                if(AppConstants.isRequired(context,episode_name)){

                }else if(friendsList.length()==0){
                    AppConstants.setToast(context,"Please select anyone of your friends");
                }else{
                    String tag = "";
                    AppConstants.hideSoftKeyboard(context, episode_name);
                    DateTimeStr = (dp.getMonth() + 1) + "/" + dp.getDayOfMonth() + "/" + dp.getYear() + " " + tp.getCurrentHour() + ":" + tp.getCurrentMinute();

                    try {
                        params = new JSONObject();
                        params.put("videofile", filePath);

                        JSONObject para1 = new JSONObject();

                        para1.put("uuid", userid);
                        if(is_camera){
                            tag = AppConstants.UPLOADIMAGE;
                        }else {
                            tag = AppConstants.UPLOAD;
                        }
                        para1.put("keywords", "");
                        para1.put("releasedate", DateTimeStr);
                        para1.put("episodename", episode_name.getText().toString());
                        para1.put("tag", "FRIENDS");
                        //here we need to get friendsID dynamically
                        para1.put("friendsId", friendsList);

                        params.put("parameters", para1);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    BackgroundTask bt = new BackgroundTask(context, tag);
                    bt.execute(AppConstants.ACTION_CREATEFRIENDSEPISODE, params.toString());
                    break;
                }
        }
    }
}
