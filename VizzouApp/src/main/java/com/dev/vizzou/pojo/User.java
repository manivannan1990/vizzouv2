package com.dev.vizzou.pojo;

/**
 * Created by ceino on 7/5/16.
 */
public class User {

    private String modifydate;

    private String lastname;

    private String viewcount;

    private String createddate;

    private String firstname;

    private String password;

    private String isactive;

    private String email;

    private String age;

    private String lattitude;

    private String gender;

    private String profilepicture;

    private String uuid;

    private String longtiutde;

    private String phoneno;

    public String getModifydate ()
    {
        return modifydate;
    }

    public void setModifydate (String modifydate)
    {
        this.modifydate = modifydate;
    }

    public String getLastname ()
    {
        return lastname;
    }

    public void setLastname (String lastname)
    {
        this.lastname = lastname;
    }

    public String getViewcount ()
    {
        return viewcount;
    }

    public void setViewcount (String viewcount)
    {
        this.viewcount = viewcount;
    }

    public String getCreateddate ()
    {
        return createddate;
    }

    public void setCreateddate (String createddate)
    {
        this.createddate = createddate;
    }

    public String getFirstname ()
    {
        return firstname;
    }

    public void setFirstname (String firstname)
    {
        this.firstname = firstname;
    }

    public String getPassword ()
    {
        return password;
    }

    public void setPassword (String password)
    {
        this.password = password;
    }

    public String getIsactive ()
    {
        return isactive;
    }

    public void setIsactive (String isactive)
    {
        this.isactive = isactive;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getAge ()
    {
        return age;
    }

    public void setAge (String age)
    {
        this.age = age;
    }

    public String getLattitude ()
    {
        return lattitude;
    }

    public void setLattitude (String lattitude)
    {
        this.lattitude = lattitude;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getProfilepicture ()
    {
        return profilepicture;
    }

    public void setProfilepicture (String profilepicture)
    {
        this.profilepicture = profilepicture;
    }

    public String getUuid ()
    {
        return uuid;
    }

    public void setUuid (String uuid)
    {
        this.uuid = uuid;
    }

    public String getLongtiutde ()
    {
        return longtiutde;
    }

    public void setLongtiutde (String longtiutde)
    {
        this.longtiutde = longtiutde;
    }

    public String getPhoneno ()
    {
        return phoneno;
    }

    public void setPhoneno (String phoneno)
    {
        this.phoneno = phoneno;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [modifydate = "+modifydate+", lastname = "+lastname+", viewcount = "+viewcount+", createddate = "+createddate+", firstname = "+firstname+", password = "+password+", isactive = "+isactive+", email = "+email+", age = "+age+", lattitude = "+lattitude+", gender = "+gender+", profilepicture = "+profilepicture+", uuid = "+uuid+", longtiutde = "+longtiutde+", phoneno = "+phoneno+"]";
    }
}
