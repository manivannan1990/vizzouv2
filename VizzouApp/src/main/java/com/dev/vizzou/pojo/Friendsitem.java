package com.dev.vizzou.pojo;

import java.io.Serializable;

/**
 * Created by KPN on 20-09-2015.
 */
public class Friendsitem implements Serializable {
    private String mEmail;
    private String mLatitude;
    private String mFirstName;
    private String mLongitude;
    private String mUserId;
    private String mLastName;
    private String mprofilepicture;
    public boolean isUsrSelected = false;

    /*public Friendsitem(String mEmail, String mLatitude, String mFirstName, String mLongitude, String mUserId, String mLastName, boolean isSelected) {

        this.mEmail = mEmail;
        this.mLatitude = mLatitude;
        this.mFirstName = mFirstName;
        this.mLongitude = mLongitude;
        this.mUserId = mUserId;
        this.mLastName = mLastName;
        this.isUsrSelected = isSelected;
    }*/

    public boolean isUsrSelected() {
        return isUsrSelected;
    }

    public void setUsrSelected(boolean isUsrSelected) {
        this.isUsrSelected = isUsrSelected;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String Email) {
        this.mEmail = Email;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String Latitude) {
        this.mLatitude = Latitude;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String FirstName) {
        this.mFirstName = FirstName;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String Longitude) {
        this.mLongitude = Longitude;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String UserId) {
        this.mUserId = UserId;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String LastName) {
        this.mLastName = LastName;
    }

    public String getProfPicture() {
        return mprofilepicture;
    }

    public void setProfPicture(String Email) {
        this.mprofilepicture = Email;
    }


}