package com.dev.vizzou.videostream;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.vizzou.R;
import com.dev.vizzou.common.AppConstants;
import com.dev.vizzou.common.BackgroundTask;
import com.dev.vizzou.common.OnTaskCompleted;

import org.json.JSONException;
import org.json.JSONObject;

import connection.AppConfig;
import io.vov.vitamio.LibsChecker;
import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.utils.Log;
import io.vov.vitamio.widget.VideoView;

public class VideoPlayerActivity extends Activity implements MediaPlayer.OnInfoListener, MediaPlayer.OnBufferingUpdateListener,OnTaskCompleted, View.OnClickListener {
    private String path = "";
    private Uri uri;
    private VideoView mVideoView;
    private ProgressBar pb;
    private TextView downloadRateView, loadRateView;
    private ImageView iv_play;
    Handler handler;
    Runnable runnable;
    private static int SPLASH_TIME_OUT = 1000;
    public boolean playing = false;
    ImageView button_close;
    ImageView toggleBtn;
    RelativeLayout toggleFrame;
    TextView joinBtn,unjoinBtn,reportBtn;
    Context context = this;
    JSONObject params;
    boolean toggleClick = false;
    String userid;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!LibsChecker.checkVitamioLibs(this))
            return;
        setContentView(R.layout.activity_video_player);
        sharedpreferences = this.getSharedPreferences(AppConfig.MYPREFERENCES, Context.MODE_PRIVATE);
        userid = sharedpreferences.getString(AppConfig.ID, "");
        mVideoView = (VideoView) findViewById(R.id.buffer);
        pb = (ProgressBar) findViewById(R.id.probar);
        iv_play = (ImageView)findViewById(R.id.iv_play);
        path = getIntent().getStringExtra("URL");
        System.out.println("Given URL=" + path);
        downloadRateView = (TextView) findViewById(R.id.download_rate);
        loadRateView = (TextView) findViewById(R.id.load_rate);
        button_close = (ImageView) findViewById(R.id.button_close);
        toggleBtn = (ImageView) findViewById(R.id.togglebtn);
        toggleFrame = (RelativeLayout) findViewById(R.id.toggleframe);
        joinBtn = (TextView) findViewById(R.id.join);
        unjoinBtn = (TextView) findViewById(R.id.unjoin);
        reportBtn = (TextView) findViewById(R.id.report);

        if(AppConstants.TYPE != null){
            if(AppConstants.TYPE.equalsIgnoreCase("JOINOPEN")  || AppConstants.TYPE.equalsIgnoreCase("UNJOINOPEN")){
                toggleBtn.setVisibility(View.VISIBLE);
            }
        }
        toggleBtn.setOnClickListener(this);
        joinBtn.setOnClickListener(this);
        unjoinBtn.setOnClickListener(this);
        reportBtn.setOnClickListener(this);
        mVideoView.setVideoLayout(VideoView.VIDEO_LAYOUT_STRETCH, 0);

        if (path == "") {
            Toast.makeText(VideoPlayerActivity.this, "Please edit VideoBuffer Activity, and set path"
                    + " variable to your media file URL/path", Toast.LENGTH_LONG).show();
            return;
        } else {

            uri = Uri.parse(path);
            mVideoView.setVideoURI(uri);
            //mVideoView.setMediaController(new MediaController(this));
            mVideoView.requestFocus();
            mVideoView.setOnInfoListener(this);
            mVideoView.setOnBufferingUpdateListener(this);
            mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {

                    System.out.println("Completed Video");
                    iv_play.setImageResource(R.drawable.ic_play);
                    iv_play.setVisibility(View.VISIBLE);
                    runafterfewminutes();
                    playing = true;

                }
            });

            iv_play.setOnClickListener(play_check);
            mVideoView.setOnTouchListener(touck_check);
            mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    // optional need Vitamio 4.0

                    mediaPlayer.setPlaybackSpeed(1.0f);
                }
            });
        }

        button_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(AppConstants.TYPE != null){
                    if(AppConstants.TYPE.equalsIgnoreCase("WATCHNOW")){
                        doRemoveWatchNowRequest(AppConstants.FE_ID,AppConstants.REFERRED_ID,AppConstants.REMOVEWATCHNOW);
                    }else {
                        VideoPlayerActivity.this.finish();
                    }
                }else {
                    VideoPlayerActivity.this.finish();
                }

            }
        });

    }
    private void doRemoveWatchNowRequest(String feid, String referredfeid, String flag){
        try {
            params = new JSONObject();
            params.put("feid", feid);
            params.put("referredfeid", referredfeid);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        BackgroundTask bt = new BackgroundTask(context,flag);
        bt.execute(AppConstants.ACTION_REMOVEWATCHNOW, params.toString());
    }

    private void doJoinOpenEpisodeRequest(String episodeid, String friendid, String flag, String usrId){
        try {
            params = new JSONObject();
            params.put("friendid", friendid);
            params.put("episodeid", episodeid);
            params.put("userid", usrId);
            params.put("referredfeid", episodeid);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        BackgroundTask bt = new BackgroundTask(context,flag);
        bt.execute(AppConstants.ACTION_JOINOPENEPISODE, params.toString());
    }

    private void doUnjoinOpenEpisodeRequest(String feid, String flag){
        try {
            params = new JSONObject();
            params.put("feid", feid);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        BackgroundTask bt = new BackgroundTask(context,flag);
        bt.execute(AppConstants.ACTION_UNJOINOPENEPISODE, params.toString());
    }

    @Override
    public void onTaskCompleted(String values, String flag) {
        Log.e("Response",values);
        try{
            if(flag.equalsIgnoreCase(AppConstants.REMOVEWATCHNOW)) {
                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")){
                    if(jsobj.getString("success").equalsIgnoreCase("true")){
                        VideoPlayerActivity.this.finish();
                        Toast.makeText(VideoPlayerActivity.this, "Video Deleted from Watch Now", Toast.LENGTH_SHORT).show();
                        toggleFrame.setVisibility(View.GONE);
                    }else {
                        Log.e("MyResponse:","fails");
                    }
                }
            }
            if(flag.equalsIgnoreCase(AppConstants.JOIN)){
                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")){
                    if(jsobj.getString("success").equalsIgnoreCase("true")){
//                        VideoPlayerActivity.this.finish();
                        Toast.makeText(VideoPlayerActivity.this, "Video is Joined to Your OpenEpsides", Toast.LENGTH_SHORT).show();
                        toggleFrame.setVisibility(View.GONE);
                    }else {
                        Log.e("MyResponse:","fails");
                    }
                }
            }
            if(flag.equalsIgnoreCase(AppConstants.UNJOIN)){
                JSONObject jsobj = new JSONObject(values);
                if(jsobj.has("success")){
                    if(jsobj.getString("success").equalsIgnoreCase("true")){
//                        VideoPlayerActivity.this.finish();
                        Toast.makeText(VideoPlayerActivity.this, "Video is Leaved from Your OpenEpsides", Toast.LENGTH_SHORT).show();
                    }else {
                        Log.e("MyResponse:","fails");
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        switch (what) {
            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                if (mVideoView.isPlaying()) {
                    mVideoView.pause();
                    pb.setVisibility(View.VISIBLE);
                    downloadRateView.setText("");
                    loadRateView.setText("");
                    downloadRateView.setVisibility(View.VISIBLE);
                    loadRateView.setVisibility(View.VISIBLE);

                }
                break;
            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                mVideoView.start();
                pb.setVisibility(View.GONE);
                downloadRateView.setVisibility(View.GONE);
                loadRateView.setVisibility(View.GONE);
                break;
            case MediaPlayer.MEDIA_INFO_DOWNLOAD_RATE_CHANGED:
                downloadRateView.setText("" + extra + "kb/s" + "  ");
                break;
        }
        return true;
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        loadRateView.setText(percent + "%");
    }


    View.OnClickListener play_check = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(mVideoView.isPlaying()){
                System.out.println("Playing so its paused");
                iv_play.setImageResource(R.drawable.ic_pause);
                iv_play.setVisibility(View.VISIBLE);
                runafterfewminutes();

                playing = false;
                mVideoView.pause();
            }

            else if(playing){
                System.out.println("restarted to play ");
                mVideoView.seekTo(0);
                mVideoView.start();

                runafterfewminutes();

            }
            else{
                System.out.println("Paused so its plays");
                iv_play.setImageResource(R.drawable.ic_pause);
                iv_play.setVisibility(View.VISIBLE);

                runafterfewminutes();
                mVideoView.start();


            }
            handler.removeCallbacks(runnable);
        }
    };

    View.OnTouchListener touck_check = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            System.out.println("Entered onTouch Listener"+mVideoView.isPlaying());
            if(mVideoView.isPlaying()){

                iv_play.setImageResource(R.drawable.ic_pause);
                iv_play.setVisibility(View.VISIBLE);
                runafterfewminutes();}
            else if(playing){
                iv_play.setImageResource(R.drawable.ic_play);
                iv_play.setVisibility(View.VISIBLE);
                runafterfewminutes();
            }
            else{
                iv_play.setImageResource(R.drawable.ic_play);
                iv_play.setVisibility(View.VISIBLE);
                runafterfewminutes();
            }

            return false;

        }
    };

    public void runafterfewminutes(){
        System.out.println("Run Ater COntrol button visible");
        if(handler == null) {
            handler = new Handler();
        }

        runnable = new Runnable() {

            @Override
            public void run() {
                iv_play.setVisibility(View.GONE);
            }
        };
        handler.postDelayed(runnable, SPLASH_TIME_OUT);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.togglebtn:
                if(toggleClick){
                    toggleFrame.setVisibility(View.GONE);
                    toggleClick = false;
                }else {
                    toggleFrame.setVisibility(View.VISIBLE);
                    toggleClick = true;
                    if(AppConstants.TYPE != null){
                        if(AppConstants.TYPE.equalsIgnoreCase("JOINOPEN"))
                            joinBtn.setVisibility(View.VISIBLE);
                        if(AppConstants.TYPE.equalsIgnoreCase("UNJOINOPEN"))
                            unjoinBtn.setVisibility(View.VISIBLE);
                    }
                }

                break;
            case R.id.join:
//                Toast.makeText(VideoPlayerActivity.this, "Join btn clickable", Toast.LENGTH_SHORT).show();
                doJoinOpenEpisodeRequest(AppConstants.EPISODE_ID,AppConstants.FRIEND_ID,AppConstants.JOIN,userid);
                break;
            case R.id.unjoin:
//                Toast.makeText(VideoPlayerActivity.this, "UnJoin btn clickable", Toast.LENGTH_SHORT).show();
                doUnjoinOpenEpisodeRequest(AppConstants.FE_ID,AppConstants.UNJOIN);
                break;
        }
    }

}

