package com.dev.vizzou;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;

import com.dev.vizzou.activities.LoginActivity;
import com.dev.vizzou.activities.MainTabActivity;
import com.dev.vizzou.activities.SplashActivity;
import com.dev.vizzou.common.AppConstants;

import butterknife.ButterKnife;
import connection.AppConfig;
import utils.ToastHandler;
import utils.VizzouBus;

public class MainActivity extends Activity {

    ToastHandler toastHandler;

    int SDK_INT = android.os.Build.VERSION.SDK_INT;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SDK_INT > 8)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here
        }
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        ButterKnife.inject(this);
        ButterKnife.inject(this);
        VizzouBus.getInstance().register(this);
        toastHandler = new ToastHandler(this);
        if (savedInstanceState == null) {
            setActivity();

            try {
                if (!(getIntent().getStringExtra(AppConstants.isLogged).equalsIgnoreCase("1"))) {
                    startActivity(new Intent(getApplicationContext(), SplashActivity.class));
                }
            }catch (Exception e){
                startActivity(new Intent(getApplicationContext(), SplashActivity.class));
            }
        }
    }

    private void setActivity() {
        try {
            SharedPreferences sharedpreferences = this.getSharedPreferences(AppConfig.MYPREFERENCES, Context.MODE_PRIVATE);
            String userid=sharedpreferences.getString(AppConfig.ID, "");

            if (userid == null || userid.equalsIgnoreCase("")) {
                Intent intent = new Intent(this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                MainActivity.this.finish();
                startActivity(intent);


            } else {
                //
                Intent intent = new Intent(this, MainTabActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                MainActivity.this.finish();
                startActivity(intent);

            }

        }catch (IllegalStateException e){
            e.printStackTrace();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }
    @Override
    public void onStart() {
        super.onStart();
    }
    @Override
    public void onStop() {
        super.onStop();
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
    @Override
    public void onDestroy() {
        VizzouBus.getInstance().unregister(this);
        super.onDestroy();
    }


}